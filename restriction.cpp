#include "restriction.h"
#include "pilot.h"
#include "shared.h"

namespace libxwing2 {

Restriction::Restriction()
  : text("")
  , restCheck([](const Pilot&, const std::vector<Pilot>&) { return std::vector<std::string> { }; })
{ }

Restriction::Restriction(std::string t, Fac f)
  : text(t)
  , restCheck([f](const Pilot& p, const std::vector<Pilot>& ps) {
      std::vector<std::string> ret;
      if((f & p.GetFaction().GetType() & Fac::All) != (p.GetFaction().GetType() & Fac::All)) {
	ret.push_back("Faction: Requires " + Faction::GetFaction(f).GetName() + " but played on " + p.GetFaction().GetName());
      }
      return ret;
    })
{ }

Restriction::Restriction(std::string t, BSz b)
  : text(t)
  , restCheck([b](const Pilot& p, const std::vector<Pilot>& ps) {
      std::vector<std::string> ret;
      if((b & p.GetShip().GetBaseSize().GetType()) != p.GetShip().GetBaseSize().GetType()) {
	ret.push_back("Base: Requires " + BaseSize::GetBaseSize(b).GetName() + " but played on " + BaseSize::GetBaseSize(b).GetName());
      }
      return ret;
    })
{ }

Restriction::Restriction(std::string t, Ships s)
  : text(t)
  , restCheck([s](const Pilot& p, const std::vector<Pilot>& ps) {
      std::vector<std::string> ret;
      std::string ships;
      bool first = true;
      for(Shp shp : s) {
	if(first) { first = false; }
	else      { ships += ", "; }
	if(p.GetShip().GetType() == shp) {
	  return ret;
	}
	ret.push_back("Ship: Requires " + ships + " but played on " + p.GetShip().GetName());
      }
      return ret;
    })
{ }

Restriction::Restriction(std::string t, SAct a)
  : text(t)
  , restCheck([a](const Pilot& p, const std::vector<Pilot>& ps) {
      std::vector<std::string> ret;
      bool hasAct = false;
      for(std::list<SAct> ac : p.GetModActions()) {
	if(ac.front() == a) {
	  hasAct = true;
	  break;
	}
      }
      if(!hasAct) {
	std::string err = "Action: Requires ";
	if(a.difficulty != Dif::All) {
	  err += Difficulty::GetDifficulty(a.difficulty).GetName();
	  err += " ";
	}
	err += Action::GetAction(a.action).GetName();
	ret.push_back(err);
      }
      return ret;
    })
{ }

Restriction::Restriction(std::string t, Fac f, SAct a)
  : text(t)
  , restCheck([f,a](const Pilot& p, const std::vector<Pilot>& ps) {
      std::vector<std::string> ret;
      if((f & p.GetFaction().GetType() & Fac::All) != (p.GetFaction().GetType() & Fac::All)) {
	ret.push_back("Faction: Requires " + Faction::GetFaction(f).GetName() + " but played on " + p.GetFaction().GetName());
      }
      bool hasAct = false;
      for(std::list<SAct> ac : p.GetModActions()) {
	if(ac.front() == a) {
	  hasAct = true;
	  break;
	}
      }
      if(!hasAct) {
	std::string err = "Action: Requires ";
	if(a.difficulty != Dif::All) {
	  err += Difficulty::GetDifficulty(a.difficulty).GetName();
	  err += " ";
	}
	err += Action::GetAction(a.action).GetName();
	ret.push_back(err);
      }
      return ret;
    })
{ }

Restriction::Restriction(std::string t, Fac f, Allowed a)
  : text(t)
  , restCheck([f,a](const Pilot& p, const std::vector<Pilot>& ps) {
      std::vector<std::string> ret;
      if(p.GetFaction().GetType() == f) { return ret; }
      std::string reqName;
      for(const Pilot& plt : ps) {
	for(Plt p2 : a.first) {
	  reqName = plt.GetName();
	  if(plt.GetType() == p2) { return ret; }
	}
	for(Upg upg : plt.GetModPossibleUpgrades()) {
	  for(Upg u2 : a.second) {
	    if(reqName == "") { reqName = Upgrade::GetUpgrade(u2).GetName(); }
	    if(upg == u2) { return ret; }
	  }
	}
      }
      ret.push_back("Faction: requires faction " + Faction::GetFaction(f).GetName() + " or squad including " + reqName);
      return ret;
    })
{ }

Restriction::Restriction(std::string t, Arcs a)
  : text(t)
  , restCheck([a](const Pilot& p, const std::vector<Pilot>& ps) {
      std::vector<std::string> ret;
      for(PriAttack pa : p.GetModAttacks()) {
	for(Arc arc : a) {
	  if(pa.arc == arc) { return ret; }
	}
      }
      ret.push_back("Attack: ");
      return ret;
    })
{ }

Restriction::Restriction(std::string t, BSz b, SAct a)
  : text(t)
  , restCheck([b,a](const Pilot& p, const std::vector<Pilot>& ps) {
      std::vector<std::string> ret;
      if((b & p.GetShip().GetBaseSize().GetType()) != p.GetShip().GetBaseSize().GetType()) {
	ret.push_back("Base: Requires " + BaseSize::GetBaseSize(b).GetName() + " but played on " + BaseSize::GetBaseSize(b).GetName());
      }
      bool hasAct = false;
      for(std::list<SAct> ac : p.GetModActions()) {
	if(ac.front() == a) {
	  hasAct = true;
	  break;
	}
      }
      if(!hasAct) {
	std::string err = "Action: Requires ";
	if(a.difficulty != Dif::All) {
	  err += Difficulty::GetDifficulty(a.difficulty).GetName();
	  err += " ";
	}
	err += Action::GetAction(a.action).GetName();
	ret.push_back(err);
      }
      return ret;
    })
{ }

Restriction::Restriction(std::string t, Fac f, Ships s)
  : text(t)
  , restCheck([f,s](const Pilot& p, const std::vector<Pilot>& ps) {
      std::vector<std::string> ret;
      if((f & p.GetFaction().GetType() & Fac::All) != (p.GetFaction().GetType() & Fac::All)) {
	ret.push_back("Faction: Requires " + Faction::GetFaction(f).GetName() + " but played on " + p.GetFaction().GetName());
      }
      std::string ships;
      bool first = true;
      for(Shp shp : s) {
	if(first) { first = false; }
	else      { ships += ", "; }
	if(p.GetShip().GetType() == shp) {
	  return ret;
	}
	ret.push_back("Ship: Requires " + ships + " but played on " + p.GetShip().GetName());
      }
      return ret;
    })
{ }

std::string Restriction::GetText() { return this->text; }
std::vector<std::string> Restriction::CheckRestrictions(const Pilot& p, const std::vector<Pilot>& ps) { return this->restCheck(p, ps); }

}
