#include "xws.h"

namespace libxwing2 {
namespace xws {

namespace faction {
  FactionNotFound::FactionNotFound(Fac f)           : runtime_error("Faction not found (enum " + std::to_string((int)f) + ")") { }
  FactionNotFound::FactionNotFound(std::string xws) : runtime_error("Faction not found '" + xws + "'") { }

  static std::vector<std::pair<Fac, std::string>> data = {
    {Fac::Rebel,      "rebel"},
    {Fac::Imperial,   "imperial"},
    {Fac::Scum,       "scum"},
    {Fac::Resistance, "resistance"},
    {Fac::FirstOrder, "firstorder"},
  };

  Fac GetFaction(std::string x) {
    for(auto s : data) {
      if(s.second == x) {
        return s.first;
      }
    }
    throw FactionNotFound(x);
  }

  std::string GetFaction(Fac f) {
    for(auto s : data) {
      if(s.first == f) {
	       return s.second;
      }
    }
    throw FactionNotFound(f);
  }

  std::vector<Fac> GetMissing() {
    std::vector<Fac> missing;
    for(Faction f : Faction::GetAllFactions()) {
      missing.push_back(f.GetType());
    }
    for(auto f : data) {
      missing.erase(std::remove(missing.begin(), missing.end(), f.first), missing.end());
    }
    return missing;
  }
}



namespace ship {
  ShipNotFound::ShipNotFound(Shp s)           : runtime_error("Ship not found (enum " + std::to_string((int)s) + ")") { }
  ShipNotFound::ShipNotFound(std::string xws) : runtime_error("Ship not found '" + xws + "'") { }

  static std::vector<std::pair<Shp, std::string>> data = {
    { Shp::Aggressor,      "aggressor",                 },
    { Shp::AlphaClass,     "alphaclassstarwing",        },
    { Shp::ARC170,         "arc170",                    },
    { Shp::AttackShuttle,  "attackshuttle",             },
    { Shp::Auzituck,       "auzituckgunship",           },
    { Shp::BWing,          "bwing",                     },
    { Shp::EscCraft,       "escapecraft"                },
    { Shp::EWing,          "ewing",                     },
    { Shp::FangFighter,    "fangfighter",               },
    { Shp::Firespray,      "firesprayclasspatrolcraft", },
    { Shp::G1A,            "g1astarfighter",            },
    { Shp::HWK290,         "hwk290",                    },
    { Shp::JM5K,           "jumpmaster5000",            },
    { Shp::Kihraxz,        "kihraxz",                   },
    { Shp::Kimogila,       "kimogila",                  },
    { Shp::KWing,          "kwing",                     },
    { Shp::Lambda,         "lambdaclassshuttle",        },
    { Shp::LancerClass,    "lancerclasspursuitcraft",   },
    { Shp::M3A,            "m3ainterceptor",            },
    { Shp::Quadjumper,     "quadjumper",                },
    { Shp::RZ1AWing,       "rz1awing",                  },
    { Shp::Scurrg,         "scurrg",                    },
    { Shp::Sheathipede,    "sheathipedeclassshuttle",   },
    { Shp::StarViper,      "starviper",                 },
    { Shp::T65XWing,       "t65xwing",                  },
    { Shp::TIEAdvV1,       "tieadvancedv1",             },
    { Shp::TIEAdvX1,       "tieadvancedx1",             },
    { Shp::TIEAggressor,   "tieaggressor",              },
    { Shp::TIEBomber,      "tiebomber",                 },
    { Shp::TIEDefender,    "tiedefender",               },
    { Shp::TIEFighter,     "tiefighter",                },
    { Shp::TIEInterceptor, "tieinterceptor",            },
    { Shp::TIEPhantom,     "tiephantom",                },
    { Shp::TIEPunisher,    "tiepunisher",               },
    { Shp::TIEReaper,      "tiereaper",                 },
    { Shp::TIEStriker,     "tiestriker",                },
    { Shp::UWing,          "uwing",                     },
    { Shp::VCX100,         "vcx100",                    },
    { Shp::VT49,           "vt49decimator",             },
    { Shp::YT1300scum,     "yt1300scum",                },
    { Shp::YT1300reb,      "yt1300reb",                 },
    { Shp::YT2400,         "yt2400",                    },
    { Shp::YV666,          "yv666",                     },
    { Shp::YWing,          "ywing",                     },
    { Shp::Z95,            "z95headhunter",             },
  };

  Shp GetShip(std::string xws) {
    for(auto s : data) {
      if(s.second == xws) {
	return s.first;
      }
    }
    throw ShipNotFound(xws);
  }

  std::string GetShip(Shp shp) {
    for(auto s : data) {
      if(s.first == shp) {
	return s.second;
      }
    }
    throw ShipNotFound(shp);
  }

  std::vector<Shp> GetMissing() {
    std::vector<Shp> missing;
    for(Ship s : Ship::GetAllShips()) {
      missing.push_back(s.GetType());
    }
    for(auto s : data) {
      missing.erase(std::remove(missing.begin(), missing.end(), s.first), missing.end());
    }
    return missing;
  }
}



namespace pilot {
  PilotNotFound::PilotNotFound(Plt     p) : runtime_error("Pilot not found (enum " + std::to_string((int)p) + ")") { }
  PilotNotFound::PilotNotFound(xwsData x) : runtime_error("Pilot not found: '" + x.pilot + "/" + x.faction + "/" + x.ship + "'") { }

  struct internalData {
    std::string pilot;
    Fac faction;
    Shp ship;
  };

  static std::vector<std::pair<Plt, internalData>> data = {

    { Plt::IG88A,           { "ig88a",                 Fac::Scum,     Shp::Aggressor } },
    { Plt::IG88B,           { "ig88b",                 Fac::Scum,     Shp::Aggressor } },
    { Plt::IG88C,           { "ig88c",                 Fac::Scum,     Shp::Aggressor } },
    { Plt::IG88D,           { "ig88d",                 Fac::Scum,     Shp::Aggressor } },

    { Plt::MajVynder,       { "majorvynder",           Fac::Imperial, Shp::AlphaClass } },
    { Plt::LtKarsabi,       { "lieutenantkarsabi",     Fac::Imperial, Shp::AlphaClass } },
    { Plt::RhoSqPlt,        { "rhosquadronpilot",      Fac::Imperial, Shp::AlphaClass } },
    { Plt::NuSqPlt,         { "nusquadronpilot",       Fac::Imperial, Shp::AlphaClass } },

    { Plt::NWexley_ARC,     { "norrawexley",           Fac::Rebel,    Shp::ARC170 } },
    { Plt::GDreis_ARC,      { "garvendreis",           Fac::Rebel,    Shp::ARC170 } },
    { Plt::SBey,            { "sbey",                  Fac::Rebel,    Shp::ARC170 } },
    { Plt::Ibtisam,         { "ibtisam",               Fac::Rebel,    Shp::ARC170 } },

    { Plt::HSyndulla_AS,    { "herasyndulla",          Fac::Rebel,    Shp::AttackShuttle } },
    { Plt::EBridger_AS,     { "ezrabridger",           Fac::Rebel,    Shp::AttackShuttle } },
    { Plt::SWren_AS,        { "sabinewren",            Fac::Rebel,    Shp::AttackShuttle } },
    { Plt::ZOrrelios_AS,    { "zeborrelios",           Fac::Rebel,    Shp::AttackShuttle } },

    { Plt::Wullffwarro,     { "wullffwarro",           Fac::Rebel,    Shp::Auzituck } },
    { Plt::Lowhhrick,       { "lowhhrick",             Fac::Rebel,    Shp::Auzituck } },
    { Plt::KashyyykDef,     { "kashyyykdefender",      Fac::Rebel,    Shp::Auzituck } },

    { Plt::BStramm,         { "braylenstramm",         Fac::Rebel,    Shp::BWing } },
    { Plt::TNumb,           { "tennumb",               Fac::Rebel,    Shp::BWing } },
    { Plt::BladeSqVet,      { "bladesquadronveteran",  Fac::Rebel,    Shp::BWing } },
    { Plt::BlueSqPlt,       { "bluesquadronpilot",     Fac::Rebel,    Shp::BWing } },

    { Plt::ORPioneer,       { "outerrimpioneer",       Fac::Scum,     Shp::EscCraft } },
    { Plt::L337E,           { "l337",                  Fac::Scum,     Shp::EscCraft } },
    { Plt::AutoPltDrone,    { "autopilotdrone",        Fac::Scum,     Shp::EscCraft } },

    { Plt::CHorn,           { "corranhorn",            Fac::Rebel,    Shp::EWing } },
    { Plt::GDarklighter,    { "gavindarklighter",      Fac::Rebel,    Shp::EWing } },
    { Plt::RogueSqEsc,      { "roguesquadronescort",   Fac::Rebel,    Shp::EWing } },
    { Plt::KnaveSqEsc,      { "knavesquadronescort",   Fac::Rebel,    Shp::EWing } },

    { Plt::FRau_FF,         { "fennrau",               Fac::Scum,     Shp::FangFighter } },
    { Plt::OTeroch,         { "oldteroch",             Fac::Scum,     Shp::FangFighter } },
    { Plt::JRekkoff,        { "joyrekkoff",            Fac::Scum,     Shp::FangFighter } },
    { Plt::KSolus,          { "kadsolus",              Fac::Scum,     Shp::FangFighter } },
    { Plt::SkullSqPlt,      { "skullsquadronpilot",    Fac::Scum,     Shp::FangFighter } },
    { Plt::ZealousRecruit,  { "zealousrecruit",        Fac::Scum,     Shp::FangFighter } },

    { Plt::BFett,           { "bobafett",              Fac::Scum,     Shp::Firespray } },
    { Plt::EAzzameen,       { "emonazzameen",          Fac::Scum,     Shp::Firespray } },
    { Plt::KScarlet,        { "kathscarlet",           Fac::Scum,     Shp::Firespray } },
    { Plt::KFrost,          { "koshkafrost",           Fac::Scum,     Shp::Firespray } },
    { Plt::KTrelix,         { "krassistrelix",         Fac::Scum,     Shp::Firespray } },
    { Plt::BountyHunter,    { "bountyhunter",          Fac::Scum,     Shp::Firespray } },

    { Plt::FourLOM,         { "4lom",                  Fac::Scum,     Shp::G1A } },
    { Plt::Zuckuss,         { "zuckuss",               Fac::Scum,     Shp::G1A } },
    { Plt::GandFindsman,    { "gandfindsman",          Fac::Scum,     Shp::G1A } },

    { Plt::JOrs,            { "janors",                Fac::Rebel,    Shp::HWK290 } },
    { Plt::RGarnet,         { "roarkgarnet",           Fac::Rebel,    Shp::HWK290 } },
    { Plt::KKatarn,         { "kylekatarn",            Fac::Rebel,    Shp::HWK290 } },
    { Plt::RebelScout,      { "rebelscout",            Fac::Rebel,    Shp::HWK290 } },

    { Plt::DBonearm,        { "dacebonearm",           Fac::Scum,     Shp::HWK290 } },
    { Plt::PGodalhi,        { "palobgodalhi",          Fac::Scum,     Shp::HWK290 } },
    { Plt::TMux,            { "torkilmux",             Fac::Scum,     Shp::HWK290 } },
    { Plt::SpiceRunner,     { "spicerunner",           Fac::Scum,     Shp::HWK290 } },

    { Plt::Dengar,          { "dengar",                Fac::Scum,     Shp::JM5K } },
    { Plt::TTrevura,        { "teltrevura",            Fac::Scum,     Shp::JM5K } },
    { Plt::Manaroo,         { "manaroo",               Fac::Scum,     Shp::JM5K } },
    { Plt::ContractedSc,    { "contractedscout",       Fac::Scum,     Shp::JM5K } },

    { Plt::TCobra,          { "talonbanecobra",        Fac::Scum,     Shp::Kihraxz } },
    { Plt::Graz,            { "graz",                  Fac::Scum,     Shp::Kihraxz } },
    { Plt::VHel,            { "viktorhel",             Fac::Scum,     Shp::Kihraxz } },
    { Plt::BlackSunAce,     { "blacksunace",           Fac::Scum,     Shp::Kihraxz } },
    { Plt::CaptJostero,     { "captainjostero",        Fac::Scum,     Shp::Kihraxz } },
    { Plt::CartMarauder,    { "cartelmarauder",        Fac::Scum,     Shp::Kihraxz } },

    { Plt::TKulda,          { "toranikulda",           Fac::Scum,     Shp::Kimogila } },
    { Plt::DOberos_KG,      { "dalanoberos",           Fac::Scum,     Shp::Kimogila } },
    { Plt::CartExecution,   { "cartelexecutioner",     Fac::Scum,     Shp::Kimogila } },

    { Plt::MDoni,           { "mirandadoni",           Fac::Rebel,    Shp::KWing } },
    { Plt::ETuketu,         { "esegetuketu",           Fac::Rebel,    Shp::KWing } },
    { Plt::WardenSqPlt,     { "wardensquadronpilot",   Fac::Rebel,    Shp::KWing } },

    { Plt::CaptKagi,        { "captainkagi",           Fac::Imperial, Shp::Lambda } },
    { Plt::ColJendon,       { "coloneljendon",         Fac::Imperial, Shp::Lambda } },
    { Plt::LtSai,           { "lieutenantsai",         Fac::Imperial, Shp::Lambda } },
    { Plt::OmicronGrpPlt,   { "omicrongrouppilot",     Fac::Imperial, Shp::Lambda } },

    { Plt::KOnyo,           { "ketsuonyo",             Fac::Scum,     Shp::LancerClass } },
    { Plt::AVentress,       { "asajjventress",         Fac::Scum,     Shp::LancerClass } },
    { Plt::SWren_LC,        { "sabinewren",            Fac::Scum,     Shp::LancerClass } },
    { Plt::ShadowportHun,   { "shadowporthunter",      Fac::Scum,     Shp::LancerClass } },

    { Plt::Serissu,         { "serissu",               Fac::Scum,     Shp::M3A } },
    { Plt::GRed,            { "genesisred",            Fac::Scum,     Shp::M3A } },
    { Plt::LAshera,         { "laetinashera",          Fac::Scum,     Shp::M3A } },
    { Plt::QJast,           { "quinnjast",             Fac::Scum,     Shp::M3A } },
    { Plt::TansariiPtVet,   { "tansariipointveteran",  Fac::Scum,     Shp::M3A } },
    { Plt::Inaldra,         { "inaldra",               Fac::Scum,     Shp::M3A } },
    { Plt::CartelSpacer,    { "cartelspacer",          Fac::Scum,     Shp::M3A } },
    { Plt::SBounder,        { "sunnybounder",          Fac::Scum,     Shp::M3A } },

    { Plt::ConstZuvio,      { "constablezuvio",        Fac::Scum,     Shp::Quadjumper } },
    { Plt::SPlank,          { "sarcoplank",            Fac::Scum,     Shp::Quadjumper } },
    { Plt::UPlutt,          { "unkarplutt",            Fac::Scum,     Shp::Quadjumper } },
    { Plt::JGunrunner,      { "jakkugunrunner",        Fac::Scum,     Shp::Quadjumper } },

    { Plt::JFarrell,        { "jakefarrell",           Fac::Rebel,    Shp::RZ1AWing } },
    { Plt::ACrynyd,         { "arvelcrynyd",           Fac::Rebel,    Shp::RZ1AWing } },
    { Plt::GreenSqPlt,      { "greensquadronpilot",    Fac::Rebel,    Shp::RZ1AWing } },
    { Plt::PhoenixSqPlt,    { "phoenixsquadronpilot",  Fac::Rebel,    Shp::RZ1AWing } },

    { Plt::CaptNym,         { "captainnym",            Fac::Scum,     Shp::Scurrg } },
    { Plt::SSixxa,          { "solsixxa",              Fac::Scum,     Shp::Scurrg } },
    { Plt::LRevenant,       { "lokrevenant",           Fac::Scum,     Shp::Scurrg } },

    { Plt::FRau_Sh,         { "fennrau",               Fac::Rebel,    Shp::Sheathipede } },
    { Plt::EBridger_Sh,     { "ezrabridger",           Fac::Rebel,    Shp::Sheathipede } },
    { Plt::ZOrrelios_Sh,    { "zeborellios",           Fac::Rebel,    Shp::Sheathipede } },
    { Plt::AP5,             { "ap5",                   Fac::Rebel,    Shp::Sheathipede } },

    { Plt::Guri,            { "guri",                  Fac::Scum,     Shp::StarViper } },
    { Plt::DOberos_SV,      { "dalanoberos",           Fac::Scum,     Shp::StarViper } },
    { Plt::PXizor,          { "princexizor",           Fac::Scum,     Shp::StarViper } },
    { Plt::BSAssassin,      { "blacksunassassin",      Fac::Scum,     Shp::StarViper } },
    { Plt::BSEnforcer,      { "blacksunenforcer",      Fac::Scum,     Shp::StarViper } },

    { Plt::WAntilles,       { "wedgeantilles",         Fac::Rebel,    Shp::T65XWing } },
    { Plt::LSkywalker,      { "lukeskywalker",         Fac::Rebel,    Shp::T65XWing } },
    { Plt::TKyrell,         { "thanekyrell",           Fac::Rebel,    Shp::T65XWing } },
    { Plt::GDreis_XW,       { "garvendreis",           Fac::Rebel,    Shp::T65XWing } },
    { Plt::JPorkins,        { "jekporkins",            Fac::Rebel,    Shp::T65XWing } },
    { Plt::KSperado,        { "kullbeesperado",        Fac::Rebel,    Shp::T65XWing } },
    { Plt::BDarklighter,    { "biggsdarklighter",      Fac::Rebel,    Shp::T65XWing } },
    { Plt::LTenza,          { "leevantenza",           Fac::Rebel,    Shp::T65XWing } },
    { Plt::RedSqVet,        { "redsquadronveteran",    Fac::Rebel,    Shp::T65XWing } },
    { Plt::BlueSqEsc,       { "bluesquadronescort",    Fac::Rebel,    Shp::T65XWing } },
    { Plt::ETwoTubes,       { "edriotwotubes",         Fac::Rebel,    Shp::T65XWing } },
    { Plt::CAZealot,        { "cavernangelszealot",    Fac::Rebel,    Shp::T65XWing } },

    { Plt::GrandInq,        { "grandinquisitor",       Fac::Imperial, Shp::TIEAdvV1 } },
    { Plt::SeventhSister,   { "seventhsister",         Fac::Imperial, Shp::TIEAdvV1 } },
    { Plt::BaronOfEmp,      { "baronoftheempire",      Fac::Imperial, Shp::TIEAdvV1 } },
    { Plt::Inq,             { "inquisitor",            Fac::Imperial, Shp::TIEAdvV1 } },

    { Plt::DVader,          { "darthvader",            Fac::Imperial, Shp::TIEAdvX1 } },
    { Plt::MStele,          { "maarekstele",           Fac::Imperial, Shp::TIEAdvX1 } },
    { Plt::VFoslo,          { "vedfoslo",              Fac::Imperial, Shp::TIEAdvX1 } },
    { Plt::ZStrom,          { "zertikstrom",           Fac::Imperial, Shp::TIEAdvX1 } },
    { Plt::StormSqAce,      { "stormsquadronace",      Fac::Imperial, Shp::TIEAdvX1 } },
    { Plt::TempestSqPlt,    { "tempestsquadronpilot",  Fac::Imperial, Shp::TIEAdvX1 } },

    { Plt::LtKestal,        { "lieutenantkestal",      Fac::Imperial, Shp::TIEAggressor } },
    { Plt::OnyxSqScout,     { "onyxsquadronscout",     Fac::Imperial, Shp::TIEAggressor } },
    { Plt::DblEdge,         { "doubleedge",            Fac::Imperial, Shp::TIEAggressor } },
    { Plt::SienSpecialist,  { "sienarspecialist",      Fac::Imperial, Shp::TIEAggressor } },

    { Plt::TBren,           { "tomaxbren",             Fac::Imperial, Shp::TIEBomber } },
    { Plt::CaptJonus,       { "captainjonus",          Fac::Imperial, Shp::TIEBomber } },
    { Plt::MajRhymer,       { "majorrhymer",           Fac::Imperial, Shp::TIEBomber } },
    { Plt::GammaSqAce,      { "gammasquadronace",      Fac::Imperial, Shp::TIEBomber } },
    { Plt::Deathfire,       { "deathfire",             Fac::Imperial, Shp::TIEBomber } },
    { Plt::ScimitarSqPlt,   { "scimitarsquadronpilot", Fac::Imperial, Shp::TIEBomber } },

    { Plt::RBrath,          { "rexlerbrath",           Fac::Imperial, Shp::TIEDefender } },
    { Plt::ColVessery,      { "colonelvessery",        Fac::Imperial, Shp::TIEDefender } },
    { Plt::CountessRyad,    { "countessryad",          Fac::Imperial, Shp::TIEDefender } },
    { Plt::OnyxSqAce,       { "onyxsquadronace",       Fac::Imperial, Shp::TIEDefender } },
    { Plt::DeltaSqPlt,      { "deltasquadronpilot",    Fac::Imperial, Shp::TIEDefender } },

    { Plt::Howlrunner,      { "howlrunner",            Fac::Imperial, Shp::TIEFighter } },
    { Plt::MMithel,         { "maulermithel",          Fac::Imperial, Shp::TIEFighter } },
    { Plt::SSkutu,          { "scourgeskutu",          Fac::Imperial, Shp::TIEFighter } },
    { Plt::DMeeko,          { "delmeeko",              Fac::Rebel,    Shp::TIEFighter } },
    { Plt::GHask,           { "gideonhask",            Fac::Rebel,    Shp::TIEFighter } },
    { Plt::IVersio,         { "idenversio",            Fac::Imperial, Shp::TIEFighter } },
    { Plt::SMarana,         { "seynmarana",            Fac::Rebel,    Shp::TIEFighter } },
    { Plt::BlackSqAce,      { "blacksquadronace",      Fac::Imperial, Shp::TIEFighter } },
    { Plt::VRudor,          { "valenrudor",            Fac::Imperial, Shp::TIEFighter } },
    { Plt::NightBeast,      { "nightbeast",            Fac::Imperial, Shp::TIEFighter } },
    { Plt::ObsidianSqPlt,   { "obsidiansquadronpilot", Fac::Imperial, Shp::TIEFighter } },
    { Plt::AcademyPlt,      { "academypilot",          Fac::Imperial, Shp::TIEFighter } },
    { Plt::Wampa,           { "wampa",                 Fac::Imperial, Shp::TIEFighter } },

    { Plt::EBridger_TF,     { "ezrabridger",           Fac::Rebel,    Shp::TIEFighter } },
    { Plt::SWren_TF,        { "sabinewren",            Fac::Rebel,    Shp::TIEFighter } },
    { Plt::ZOrrelios_TF,    { "zeborrelios",           Fac::Rebel,    Shp::TIEFighter } },
    { Plt::CaptRex,         { "captainrex",            Fac::Rebel,    Shp::TIEFighter } },

    { Plt::SFel,            { "soontirfel",            Fac::Imperial, Shp::TIEInterceptor } },
    { Plt::SaberSqAce,      { "sabersquadronace",      Fac::Imperial, Shp::TIEInterceptor } },
    { Plt::TPhennir,        { "turrphennir",           Fac::Imperial, Shp::TIEInterceptor } },
    { Plt::AlphaSqPlt,      { "alphasquadronpilot",    Fac::Imperial, Shp::TIEInterceptor } },

    { Plt::Whisper,         { "whisper",               Fac::Imperial, Shp::TIEPhantom } },
    { Plt::Echo,            { "echo",                  Fac::Imperial, Shp::TIEPhantom } },
    { Plt::SigmaSqAce,      { "sigmasquadronace",      Fac::Imperial, Shp::TIEPhantom } },
    { Plt::ImdaarTestPlt,   { "imdaartestpilot",       Fac::Imperial, Shp::TIEPhantom } },

    { Plt::Redline,         { "redline",               Fac::Imperial, Shp::TIEPunisher } },
    { Plt::Deathrain,       { "deathrain",             Fac::Imperial, Shp::TIEPunisher } },
    { Plt::CutlassSqPlt,    { "cutlasssquadronpilot",  Fac::Imperial, Shp::TIEPunisher } },

    { Plt::MajVermeil,      { "majorvermeil",          Fac::Imperial, Shp::TIEReaper } },
    { Plt::CaptFeroph,      { "captainferoph",         Fac::Imperial, Shp::TIEReaper } },
    { Plt::Vizier,          { "vizier",                Fac::Imperial, Shp::TIEReaper } },
    { Plt::ScarifBsPlt,     { "scarifbasepilot",       Fac::Imperial, Shp::TIEReaper } },

    { Plt::Duchess,         { "duchess",               Fac::Imperial, Shp::TIEStriker } },
    { Plt::Countdown,       { "countdown",             Fac::Imperial, Shp::TIEStriker } },
    { Plt::PureSabacc,      { "puresabacc",            Fac::Imperial, Shp::TIEStriker } },
    { Plt::BlackSqScout,     { "blacksqscout",          Fac::Imperial, Shp::TIEStriker } },
    { Plt::PlanetarySent,   { "planetarysentinel",     Fac::Imperial, Shp::TIEStriker } },

    { Plt::BRook,           { "bodhirook",             Fac::Rebel,    Shp::UWing } },
    { Plt::SGerrera,        { "sawgerrera",            Fac::Rebel,    Shp::UWing } },
    { Plt::CAndor,          { "cassianandor",          Fac::Rebel,    Shp::UWing } },
    { Plt::MYarro,          { "magvayarp",             Fac::Rebel,    Shp::UWing } },
    { Plt::BTwoTubes,       { "benthictwotubes",       Fac::Rebel,    Shp::UWing } },
    { Plt::BlueSqScout,     { "bluesquadronscout",     Fac::Rebel,    Shp::UWing } },
    { Plt::HTobber,         { "hefftobber",            Fac::Rebel,    Shp::UWing } },
    { Plt::PartRenegade,    { "partisanrenegade",      Fac::Rebel,    Shp::UWing } },

    { Plt::HSyndulla_VCX,   { "herasyndulla",          Fac::Rebel,    Shp::VCX100 } },
    { Plt::KJarrus,         { "kananjarrus",           Fac::Rebel,    Shp::VCX100 } },
    { Plt::Chopper,         { "chopper",               Fac::Rebel,    Shp::VCX100 } },
    { Plt::LothalRebel,     { "lothalrebel",           Fac::Rebel,    Shp::VCX100 } },

    { Plt::RAdmChiraneau,   { "rearadmiralchiraneau",  Fac::Imperial, Shp::VT49 } },
    { Plt::CaptOicunn,      { "captainoicunn",         Fac::Imperial, Shp::VT49 } },
    { Plt::PatrolLdr,       { "patrolleader",          Fac::Imperial, Shp::VT49 } },

    { Plt::HSoloScum,       { "hansolo",               Fac::Scum,     Shp::YT1300scum } },
    { Plt::LCalrissianScum, { "landocalrissian",       Fac::Scum,     Shp::YT1300scum } },
    { Plt::L337C,           { "l337"           ,       Fac::Scum,     Shp::YT1300scum } },

    { Plt::HSoloReb,        { "hansolo",               Fac::Rebel,    Shp::YT1300reb } },
    { Plt::LCalrissianReb,  { "landocalrissian",       Fac::Rebel,    Shp::YT1300reb } },
    { Plt::Chewbacca,       { "chewbacca",             Fac::Rebel,    Shp::YT1300reb } },
    { Plt::OuterRimSmug,    { "outerrimsmuggler",      Fac::Rebel,    Shp::YT1300reb } },

    { Plt::DRendar,         { "dashrendar",            Fac::Rebel,    Shp::YT2400 } },
    { Plt::Leebo,           { "leebo",                 Fac::Rebel,    Shp::YT2400 } },
    { Plt::WildSpaceFrin,   { "wildspacefringer",      Fac::Rebel,    Shp::YT2400 } },

    { Plt::Bossk,           { "bossk",                 Fac::Scum,     Shp::YV666 } },
    { Plt::MEval,           { "moraloeval",            Fac::Scum,     Shp::YV666 } },
    { Plt::LRazzi,          { "lattsrazzi",            Fac::Scum,     Shp::YV666 } },
    { Plt::TSlaver,         { "trandoshanslaver",      Fac::Scum,     Shp::YV666 } },

    { Plt::NWexley_YW,      { "norrawexley",           Fac::Rebel,    Shp::YWing } },
    { Plt::DVander,         { "dutchvander",           Fac::Rebel,    Shp::YWing } },
    { Plt::HSalm,           { "hortonsalm",            Fac::Rebel,    Shp::YWing } },
    { Plt::EVerlaine,       { "evaanverlaine",         Fac::Rebel,    Shp::YWing } },
    { Plt::GoldSqVet,       { "goldsquadronveteran",   Fac::Rebel,    Shp::YWing } },
    { Plt::GraySqBomber,    { "graysquadronbomber",    Fac::Rebel,    Shp::YWing } },

    { Plt::Kavil,           { "kavil",                 Fac::Scum,     Shp::YWing } },
    { Plt::DRenthal,        { "drearenthal",           Fac::Scum,     Shp::YWing } },
    { Plt::HiredGun,        { "hiredgun",              Fac::Scum,     Shp::YWing } },
    { Plt::CrymorahGoon,    { "crymorahgoon",          Fac::Scum,     Shp::YWing } },

    { Plt::ACracken,        { "airencracken",          Fac::Rebel,    Shp::Z95 } },
    { Plt::LtBlount,        { "lieutenantblount",      Fac::Rebel,    Shp::Z95 } },
    { Plt::TalaSqPlt,       { "talasquadronpilot",     Fac::Rebel,    Shp::Z95 } },
    { Plt::BanditSqPlt,     { "banditsquadronpilot",   Fac::Rebel,    Shp::Z95 } },

    { Plt::NSuhlak,         { "ndrusuhlak",            Fac::Scum,     Shp::Z95 } },
    { Plt::BlackSunSoldier, { "blacksunsoldier",       Fac::Scum,     Shp::Z95 } },
    { Plt::KLeeachos,       { "kaatoleeachos",         Fac::Scum,     Shp::Z95 } },
    { Plt::BinayrePirate,   { "binayrepirate",         Fac::Scum,     Shp::Z95 } },
    { Plt::NashtahPup,      { "nashtahpup",            Fac::Scum,     Shp::Z95 } },
  };

  Plt GetPilot(xwsData x) {
    for(auto p : data) {
      Fac fac = faction::GetFaction(x.faction);
      Shp shp = ship::GetShip(x.ship);
      if((p.second.pilot==x.pilot) &&
      	 (p.second.faction==fac) &&
	 (p.second.ship==shp)) {
	return p.first;
      }
    }
    throw PilotNotFound(x);
  }

  xwsData GetPilot(Plt plt) {
    for(auto p : data) {
      if(p.first == plt) {
    	return { p.second.pilot, faction::GetFaction(p.second.faction), ship::GetShip(p.second.ship) };
      }
    }
    throw PilotNotFound(plt);
  }

  std::vector<Plt> GetMissing() {
    std::vector<Plt> missing;
    for(Pilot p : Pilot::GetAllPilots()) {
      missing.push_back(p.GetType());
    }
    for(auto p : data) {
      missing.erase(std::remove(missing.begin(), missing.end(), p.first), missing.end());
    }
    return missing;
  }
}



namespace upgradetype {
  UpgradeTypeNotFound::UpgradeTypeNotFound(UpT         u) : runtime_error("UpgradeType not found (enum " + std::to_string((int)u) + ")") { }
  UpgradeTypeNotFound::UpgradeTypeNotFound(std::string x) : runtime_error("UpgradeType not found: '" + x + "'") { }

  static std::vector<std::pair<UpT, std::string>> data = {
    { UpT::Astromech,    "amd"     },
    { UpT::Cannon,       "cannon"  },
    { UpT::Config,       "config"  },
    { UpT::Crew,         "crew"    },
    { UpT::Device,       "device"  },
    { UpT::Force,        "force"   },
    { UpT::Gunner,       "gunner"  },
    { UpT::Illicit,      "illicit" },
    { UpT::Missile,      "missile" },
    { UpT::Modification, "mod"     },
    { UpT::System,       "system"  },
    { UpT::Talent,       "talent"  },
    { UpT::Title,        "title"   },
    { UpT::Torpedo,      "torpedo" },
    { UpT::Turret,       "turret"  },
  };

  UpT GetUpgradeType(std::string x) {
    for(auto s : data) {
      if(s.second == x) {
        return s.first;
      }
    }
    throw UpgradeTypeNotFound(x);

  }

  std::string GetUpgradeType(UpT u) {
    for(auto s : data) {
      if(s.first == u) {
	return s.second;
      }
    }
    throw UpgradeTypeNotFound(u);
  }

  std::vector<UpT> GetMissing() {
    std::vector<UpT> missing;
    for(UpgradeType u : UpgradeType::GetAllUpgradeTypes()) {
      missing.push_back(u.GetType());
    }
    for(auto u : data) {
      missing.erase(std::remove(missing.begin(), missing.end(), u.first), missing.end());
    }
    return missing;
  }
}



namespace upgrade {
  UpgradeNotFound::UpgradeNotFound(Upg     u) : runtime_error("Upgrade not found (enum " + std::to_string((int)u) + ")") { }
  UpgradeNotFound::UpgradeNotFound(xwsData x) : runtime_error("Upgrade not found: '" + x.type + "/" + x.name + "'") { }

  struct internalData {
    UpT         type;
    std::string name;
  };

  static std::vector<std::pair<Upg, internalData>> data = {
    { Upg::ChopperA,        { UpT::Astromech,    "chopper" } },
    { Upg::Genius,          { UpT::Astromech,    "genius" } },
    { Upg::R2Astro,         { UpT::Astromech,    "r2astromech" } },
    { Upg::R2D2 ,           { UpT::Astromech,    "r2d2" } },
    { Upg::R3Astro,         { UpT::Astromech,    "r3astromech" } },
    { Upg::R4Astro,         { UpT::Astromech,    "r4astromech" } },
    { Upg::R5Astro,         { UpT::Astromech,    "r5astromech" } },
    { Upg::R5D8,            { UpT::Astromech,    "r5d8" } },
    { Upg::R5P8,            { UpT::Astromech,    "r5p8" } },
    { Upg::R5TK,            { UpT::Astromech,    "r5tk" } },

    { Upg::HLC,             { UpT::Cannon,       "heavylasercannon" } },
    { Upg::IonCan,          { UpT::Cannon,       "ioncaonnon" } },
    { Upg::JamBeam,         { UpT::Cannon,       "jammingbeam" } },
    { Upg::TracBeam,        { UpT::Cannon,       "tractorbeam" } },

    { Upg::Os1ArsenalLdt,   { UpT::Config,       "os1arsenalloadout" } },
    { Upg::PvtWing,         { UpT::Config,       "pivotwing" } },
    { Upg::SFoils,          { UpT::Config,       "servomotorsfoils" } },
    { Upg::Xg1AssaultCfg,   { UpT::Config,       "xg1assaultconfig" } },

    { Upg::AdmSloane,       { UpT::Crew,         "admiralsloane" } },
    { Upg::AgentKallus,     { UpT::Crew,         "agentkallus" } },
    { Upg::BFett,           { UpT::Crew,         "bobafett" } },
    { Upg::BMalbus,         { UpT::Crew,         "bazemalbus" } },
    { Upg::C3PO,            { UpT::Crew,         "c3po" } },
    { Upg::CAndor,          { UpT::Crew,         "cassianandor" } },
    { Upg::CBane,           { UpT::Crew,         "cadbane" } },
    { Upg::ChewieReb,       { UpT::Crew,         "chewbacca" } },
    { Upg::ChewieScum,      { UpT::Crew,         "chewbaccascum" } },
    { Upg::ChopperC,        { UpT::Crew,         "chopper" } },
    { Upg::CRee,            { UpT::Crew,         "cienaree" } },
    { Upg::CVizago,         { UpT::Crew,         "cikatrovizago" } },
    { Upg::DVader,          { UpT::Crew,         "darthvader" } },
    { Upg::DeathTr,         { UpT::Crew,         "deathtroopers" } },
    { Upg::DirKrennic,      { UpT::Crew,         "directorkrennic" } },
    { Upg::EmpPalp,         { UpT::Crew,         "emperorpalpatine" } },
    { Upg::FLSlicer,        { UpT::Crew,         "freelanceslicer" } },
    { Upg::FourLOM,         { UpT::Crew,         "4lom" } },
    { Upg::Gonk,            { UpT::Crew,         "gnkgonkdroid" } },
    { Upg::GrandInq,        { UpT::Crew,         "grandinquisitor" } },
    { Upg::GrandMoffTarkin, { UpT::Crew,         "grandmofftarkin" } },
    { Upg::HSolo,           { UpT::Crew,         "hansolo" } },
    { Upg::HSyndulla,       { UpT::Crew,         "herasyndulla" } },
    { Upg::IG88D,           { UpT::Crew,         "ig88d" } },
    { Upg::Informant,       { UpT::Crew,         "informant" } },
    { Upg::ISBSlicer,       { UpT::Crew,         "isbslicer" } },
    { Upg::Jabba,           { UpT::Crew,         "jabbathehutt" } },
    { Upg::JErso,           { UpT::Crew,         "jynerso" } },
    { Upg::KJarrus,         { UpT::Crew,         "kananjarrus" } },
    { Upg::KOnyo,           { UpT::Crew,         "ketsuonyo" } },
    { Upg::L337,            { UpT::Crew,         "l337" } },
    { Upg::LCalrissianReb,  { UpT::Crew,         "landocalrissian" } },
    { Upg::LCalrissianScum, { UpT::Crew,         "landocalrissianscum" } },
    { Upg::LOrgana,         { UpT::Crew,         "leiaorgana" } },
    { Upg::LRazzi,          { UpT::Crew,         "lattsrazzi" } },
    { Upg::Maul,            { UpT::Crew,         "maul" } },
    { Upg::MinTua,          { UpT::Crew,         "ministertua" } },
    { Upg::MoffJerjerrod,   { UpT::Crew,         "moffjerjerrod" } },
    { Upg::MYarro,          { UpT::Crew,         "magvayarro" } },
    { Upg::NNunb,           { UpT::Crew,         "niennunb" } },
    { Upg::NovTech,         { UpT::Crew,         "novicetechnician" } },
    { Upg::PerCPilot,       { UpT::Crew,         "perceptivecopilot" } },
    { Upg::Qira,            { UpT::Crew,         "qira" } },
    { Upg::R2D2C,           { UpT::Crew,         "r2d2" } },
    { Upg::SWren,           { UpT::Crew,         "sabinewren" } },
    { Upg::SGerrera,        { UpT::Crew,         "sawgerrera" } },
    { Upg::SeasonNav,       { UpT::Crew,         "seasonednavigator" } },
    { Upg::SeventhSister,   { UpT::Crew,         "seventhsister" } },
    { Upg::TactOff,         { UpT::Crew,         "tacticalofficer" } },
    { Upg::TripleZero,      { UpT::Crew,         "000" } },
    { Upg::TBeckett,        { UpT::Crew,         "tobiasbeckett" } },
    { Upg::UPlutt,          { UpT::Crew,         "unkarplutt" } },
    { Upg::ZOrrelios,       { UpT::Crew,         "zeborrelios" } },
    { Upg::Zuckuss,         { UpT::Crew,         "zuckuss" } },

    { Upg::Bomblet,         { UpT::Device,       "bombletgenerator" } },
    { Upg::ConnerNet,       { UpT::Device,       "connernets" } },
    { Upg::ProtonBmb,       { UpT::Device,       "protombombs" } },
    { Upg::ProxMine,        { UpT::Device,       "proximitymines" } },
    { Upg::SeismicCh,       { UpT::Device,       "seismiccharges" } },

    { Upg::HeightPerc,      { UpT::Force,        "heightenedperception" } },
    { Upg::InstAim,         { UpT::Force,        "instinctiveaim" } },
    { Upg::Sense,           { UpT::Force,        "sense" } },
    { Upg::SNReflex,        { UpT::Force,        "supernaturalreflexes" } },

    { Upg::Bistan,          { UpT::Gunner,       "bistan" } },
    { Upg::Bossk,           { UpT::Gunner,       "bossk" } },
    { Upg::BT1,             { UpT::Gunner,       "bt1" } },
    { Upg::Dengar,          { UpT::Gunner,       "dengar" } },
    { Upg::EBridger,        { UpT::Gunner,       "ezrabridger" } },
    { Upg::FifthBrother,    { UpT::Gunner,       "fifthbrother" } },
    { Upg::Greedo,          { UpT::Gunner,       "greedo" } },
    { Upg::HSoloG,          { UpT::Gunner,       "hansolo" } },
    { Upg::HsGunner,        { UpT::Gunner,       "hotshotgunner" } },
    { Upg::LSkywalkerG,     { UpT::Gunner,       "lukeskywalker" } },
    { Upg::SkBombard,       { UpT::Gunner,       "skilledbombardier" } },
    { Upg::VetTailGun,      { UpT::Gunner,       "veterantailgunner" } },
    { Upg::VetTurret,       { UpT::Gunner,       "veteranturretgunner" } },

    { Upg::CloakDev,        { UpT::Illicit,      "cloakingdeview" } },
    { Upg::ContraCyb,       { UpT::Illicit,      "contrabandcybernetics" } },
    { Upg::DeadmansSw,      { UpT::Illicit,      "deadmansswitch" } },
    { Upg::FeedbackAr,      { UpT::Illicit,      "feedbackarray" } },
    { Upg::InertDamp,       { UpT::Illicit,      "inertialdampeners" } },
    { Upg::RigCargoCh,      { UpT::Illicit,      "riggedcargochute" } },

    { Upg::AblatPlat,       { UpT::Modification, "ablativeplating" } },
    { Upg::AdvSLAM,         { UpT::Modification, "advancedslam" } },
    { Upg::Afterburn,       { UpT::Modification, "afterburner" } },
    { Upg::ElectBaff,       { UpT::Modification, "electronicbaffle" } },
    { Upg::EngUpg,          { UpT::Modification, "engineupgrade" } },
    { Upg::HullUpg,         { UpT::Modification, "hullupgrade" } },
    { Upg::MuniFailSa,      { UpT::Modification, "munitionsfailsafe" } },
    { Upg::StaDcVanes,      { UpT::Modification, "staticdischargevanes" } },
    { Upg::ShieldUpg,       { UpT::Modification, "shieldupgrade" } },
    { Upg::StealthDev,      { UpT::Modification, "stealthdevice" } },
    { Upg::TactScramb,      { UpT::Modification, "tacticalscrambler" } },

    { Upg::BarRockets,      { UpT::Missile,      "barragerockets" } },
    { Upg::ClustMsl,        { UpT::Missile,      "clustermissiles" } },
    { Upg::ConcusMsl,       { UpT::Missile,      "concussionmissiles" } },
    { Upg::HomingMsl,       { UpT::Missile,      "homingmissiles" } },
    { Upg::IonMsl,          { UpT::Missile,      "ionmissiles" } },
    { Upg::PRockets,        { UpT::Missile,      "protonrockets" } },

    { Upg::AdvSensors,      { UpT::System,       "advancedsensors" } },
    { Upg::CollisDet,       { UpT::System,       "collisiondetector" } },
    { Upg::FCS,             { UpT::System,       "firecontrolsystem" } },
    { Upg::TrajSim,         { UpT::System,       "trajectorysimulator" } },

    { Upg::CrackShot,       { UpT::Talent,       "crackshot" } },
    { Upg::Daredevil,       { UpT::Talent,       "daredevil" } },
    { Upg::DebGambit,       { UpT::Talent,       "debrisgambit" } },
    { Upg::Elusive,         { UpT::Talent,       "elusive" } },
    { Upg::ExpHan,          { UpT::Talent,       "experthandling" } },
    { Upg::Fearless,        { UpT::Talent,       "fearless" } },
    { Upg::Intimidat,       { UpT::Talent,       "intimidation" } },
    { Upg::Juke,            { UpT::Talent,       "juke" } },
    { Upg::LoneWolf,        { UpT::Talent,       "lonewolf" } },
    { Upg::Marksman,        { UpT::Talent,       "marksmanship" } },
    { Upg::Outmaneuv,       { UpT::Talent,       "outmaneuver" } },
    { Upg::Predator,        { UpT::Talent,       "prepdator" } },
    { Upg::Ruthless,        { UpT::Talent,       "ruthless" } },
    { Upg::SatSalvo,        { UpT::Talent,       "saturationsalvo" } },
    { Upg::Selfless,        { UpT::Talent,       "selfless" } },
    { Upg::SquadLdr,        { UpT::Talent,       "squadleader" } },
    { Upg::SwarmTac,        { UpT::Talent,       "swarmtactics" } },
    { Upg::TrickSh,         { UpT::Talent,       "trickshot" } },

    { Upg::Andrasta,        { UpT::Title,        "andrasta" } },
    { Upg::Dauntless,       { UpT::Title,        "dauntless" } },
    { Upg::Ghost,           { UpT::Title,        "ghost" } },
    { Upg::Havoc,           { UpT::Title,        "havoc" } },
    { Upg::HoundsTooth,     { UpT::Title,        "houndstooth" } },
    { Upg::IG2000,          { UpT::Title,        "ig2000" } },
    { Upg::LandosMF,        { UpT::Title,        "landosmillenniumfalcon" } },
    { Upg::Marauder,        { UpT::Title,        "marauder" } },
    { Upg::MilFalcon,       { UpT::Title,        "millenniumfalcon" } },
    { Upg::MistHunter,      { UpT::Title,        "misthunter" } },
    { Upg::MoldyCrow,       { UpT::Title,        "moldycrow" } },
    { Upg::Outrider,        { UpT::Title,        "outrider" } },
    { Upg::Phantom,         { UpT::Title,        "phantom" } },
    { Upg::PunishingOne,    { UpT::Title,        "punishingone" } },
    { Upg::ShadowCaster,    { UpT::Title,        "shadowcaster" } },
    { Upg::SlaveI,          { UpT::Title,        "slavei" } },
    { Upg::ST321,           { UpT::Title,        "st321" } },
    { Upg::Virago,          { UpT::Title,        "virago" } },

    { Upg::AdvProtTrp,      { UpT::Torpedo,      "advancedprotontorpedoes" } },
    { Upg::IonTrp,          { UpT::Torpedo,      "iontorpedoes" } },
    { Upg::ProtTrp,         { UpT::Torpedo,      "protontorpedoes" } },

    { Upg::DorsalTrt,       { UpT::Turret,       "dorsalturret" } },
    { Upg::IonCanTrt,       { UpT::Turret,       "ioncannonturret" } },

  };

  Upg GetUpgrade(xwsData x) {
    for(auto u : data) {
      UpT typ = upgradetype::GetUpgradeType(x.type);
      if((u.second.type==typ) &&
      	 (u.second.name==x.name)) {
	return u.first;
      }
    }
    throw UpgradeNotFound(x);
  }

  xwsData GetUpgrade(Upg upg) {
    for(auto u : data) {
      if(u.first == upg) {
    	return { upgradetype::GetUpgradeType(u.second.type), u.second.name };
      }
    }
    throw UpgradeNotFound(upg);
  }

  std::vector<Upg> GetMissing() {
    std::vector<Upg> missing;
    for(Upgrade u : Upgrade::GetAllUpgrades()) {
      missing.push_back(u.GetType());
    }
    for(auto u : data) {
      missing.erase(std::remove(missing.begin(), missing.end(), u.first), missing.end());
    }
    return missing;
  }
};



namespace condition {
  ConditionNotFound::ConditionNotFound(Cnd         c) : runtime_error("Condition not found (enum " + std::to_string((int)c) + ")") { }
  ConditionNotFound::ConditionNotFound(std::string x) : runtime_error("Condition not found: '" + x + "'") { }

  static std::vector<std::pair<Cnd, std::string>> data = {
    { Cnd::Hunted,  "hunted"             },
    { Cnd::LstnDev, "listeningdevice"    },
    { Cnd::OptProt, "optimizedprototype" },
    { Cnd::SupFire, "suppressivefire"    },
  };

  Cnd GetCondition(std::string x) {
    for(auto s : data) {
      if(s.second == x) {
        return s.first;
      }
    }
    throw ConditionNotFound(x);

  }

  std::string GetCondition(Cnd c) {
    for(auto s : data) {
      if(s.first == c) {
	return s.second;
      }
    }
    throw ConditionNotFound(c);
  }

  std::vector<Cnd> GetMissing() {
    std::vector<Cnd> missing;
    for(Condition c : Condition::GetAllConditions()) {
      missing.push_back(c.GetType());
    }
    for(auto c : data) {
      missing.erase(std::remove(missing.begin(), missing.end(), c.first), missing.end());
    }
    return missing;
  }
}

}}
