#DEBUG=-g
CPP=clang++
CPPFLAGS=$(DEBUG) -Wall -fPIC -std=c++14

OBJ=jsoncpp.o action.o attack.o basesize.o chargeable.o condition.o damagedeck.o dice.o difficulty.o faction.o glyph.o maneuver.o modifier.o pilot.o pilotdb.o quickbuild.o release.o releasedb.o restriction.o shared.o ship.o shipdb.o token.o upgrade.o upgradedb.o upgradetype.o xws.o

all: libxwing2.a

libxwing2.a: $(OBJ)
	ar cr libxwing2.a $(OBJ)

jsoncpp.o: jsoncpp.cpp json/json.h
action.o: action.cpp action.h
#condition.o: condition.cpp condition.h
amagedeck.o : damagedeck.cpp damagedeck.h
#dice.o: dice.cpp dice.h
faction.o: faction.cpp faction.h
#glyph.o: glyph.cpp glyph.h ship.h
modifier.o: modifier.cpp modifier.h
pilot.o: pilot.cpp pilot.h shared.h
#pilotdb.o: pilotdb.cpp pilot.h shared.h
#quickbuild.o: quickbuild.cpp quickbuild.h
release.o: release.cpp release.h
releasedb.o: releasedb.cpp release.h
restriction.o: restriction.cpp restriction.h
shared.o: shared.cpp shared.h
ship.o: ship.cpp ship.h
shipdb.o: shipdb.cpp ship.h
#squad.o: squad.cpp squad.h shared.h
#test.o: test.cpp test.h
#token.o : token.cpp token.h
#upgrade.o: upgrade.cpp upgrade.h shared.h
#upgradedb.o: upgradedb.cpp upgrade.h shared.h

clean:
	rm -rf *.o *~ libxwing2.dSYM libxwing2.a

.SUFFIXES: .cpp .o
.cpp.o:
	$(CPP) $(CPPFLAGS) -c $<
