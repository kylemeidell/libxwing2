#include "ship.h"

namespace libxwing2 {

typedef Maneuvers  M;

const Brn LT = Brn::LTurn;
const Brn LB = Brn::LBank;
const Brn ST = Brn::Straight;
const Brn RB = Brn::RBank;
const Brn RT = Brn::RTurn;
const Brn KT = Brn::KTurn;
const Brn SY = Brn::Stationary;
const Brn LS = Brn::LSloop;
const Brn RS = Brn::RSloop;
const Brn LR = Brn::LTroll;
const Brn RR = Brn::RTroll;
const Brn RLB= Brn::RevLBank;
const Brn RST= Brn::RevStraight;
const Brn RRB= Brn::RevRBank;

const Dif B = Dif::Blue;
const Dif W = Dif::White;
const Dif R = Dif::Red;


// maneuvers          5                   4                             3 Standard                          3 Special                         2 Standard                               2 Special                              1 Standard                         1 Special          0
//           ------------------  ------------------  ------------------------------------------------  ------------------  ------------------------------------------------  ----------------------------  ------------------------------------------------  ------------------  --------
M mAGGRE = {                     {4,ST,W}, {4,KT,R},           {3,LB,B}, {3,ST,B}, {3,RB,B}, {3,LS,R}, {3,RS,R},           {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,W}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,W}                               };
M mALPHA = {                     {4,ST,R},           {3,LT,W}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,W},                     {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,W}, {1,ST,B}, {1,RB,W}                                         };
M mARC17 = {                     {4,ST,R}, {4,KT,R}, {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B}                                         };
M mATTSH = {                     {4,ST,W}, {4,KT,R}, {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                               {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R}                               };
M mAUZIT = {                     {4,ST,W},           {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B},                               {0,SY,R} };
M mBWING = {                     {4,ST,R},                     {3,LB,R}, {3,ST,B}, {3,RB,R},                               {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W}, {2,KT,R},                     {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R}, {1,LR,R}, {1,RR,R}           };
M mESCCR = {                                                   {3,LB,W}, {3,ST,W}, {3,RB,W},           {3,KT,R},           {2,LT,R}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,R},                                                   {1,LB,B}, {1,ST,B}, {1,RB,B},                     {0,SY,R} };
M mEWING = { {5,ST,W},           {4,ST,B}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,LS,R}, {3,RS,R}, {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                               {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R}                               };
M mFANGF = { {5,ST,W},           {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,B}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,B}, {2,LR,R}, {2,RR,R},           {1,LT,W},                               {1,RT,W}                               };
M mFIRES = {                     {4,ST,W}, {4,KT,R},           {3,LB,W}, {3,ST,B}, {3,RB,W},           {3,LR,R}, {3,RR,R}, {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                               {1,LT,W}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,W}                               };
M mG1ASF = {                     {4,ST,R}, {4,KT,R},           {3,LB,R}, {3,ST,W}, {3,RB,R},                               {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W}, {2,KT,R},                     {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R},                     {0,SY,R} };
M mHWK29 = {                     {4,ST,W},           {3,LT,R}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B},                               {0,SY,R} };
M mJUM5K = {                     {4,ST,W}, {4,KT,R},           {3,LB,B}, {3,ST,B}, {3,RB,W},           {3,LS,R},           {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,W}, {2,RT,R},                               {1,LT,W}, {1,LB,B}, {1,ST,B}, {1,RB,W}, {1,RT,R}                               };
M mKIHRA = {                     {4,ST,B}, {4,KT,R},           {3,LB,W}, {3,ST,B}, {3,RB,W},                               {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W}, {2,LR,R}, {2,RR,R},           {1,LT,W}, {1,LB,B},           {1,RB,B}, {1,RT,W}                               };
M mKIMOG = {                               {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,R}, {1,LB,W}, {1,ST,B}, {1,RB,W}, {1,RT,R}                               };
M mKWING = {                                                   {3,LB,W}, {3,ST,W}, {3,RB,W},                               {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B}                                         };
M mLAMBD = {                                                   {3,LB,R}, {3,ST,W}, {3,RB,R},                               {2,LT,R}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,R},                                         {1,LB,B}, {1,ST,B}, {1,RB,B},                               {0,SY,R} };
M mLACPC = { {5,ST,W}, {5,KT,R}, {4,ST,B},           {3,LT,B}, {3,LB,B}, {3,ST,B}, {3,RB,B}, {3,RT,B},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,W}, {1,ST,W}, {1,RB,W}                                         };
M mM3AIN = { {5,ST,W}, {5,KT,R}, {4,ST,B},                     {3,LB,W}, {3,ST,B}, {3,RB,W},           {3,KT,R},           {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                               {1,LT,W}, {1,LB,B},           {1,RB,B}, {1,RT,W}                               };
M mQUADJ = {                                                   {3,LB,W}, {3,ST,B}, {3,RB,W},                               {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W}, {2,LS,R}, {2,RS,R},{2,RST,R}, {1,LT,W}, {1,LB,W}, {1,ST,W}, {1,RB,W}, {1,RT,W}, {1,RLB,R},{1,RRB,R}          };
M mRZ1AW = { {5,ST,B}, {5,KT,R}, {4,ST,B},           {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,LS,R}, {3,RS,R}, {2,LT,B}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,B},                               {1,LT,W},                               {1,RT,W}                               };
M mSCURR = {                     {4,ST,R},           {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R}, {3,LR,R}, {3,RR,R}, {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B}                                         };
M mSHEAT = {                     {4,ST,R},           {3,LT,R}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,R}, {3,KT,R},           {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,W}, {1,ST,B}, {1,RB,W},           {1,RST,R}                    };
M mSTARV = {                     {4,ST,W},                     {3,LB,W}, {3,ST,B}, {3,RB,W},           {3,LS,R}, {3,RS,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,W}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,W}                               };
M mT65XW = {                     {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,W}, {3,LR,R}, {3,RR,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B}                                         };
M mTIEAv = { {5,ST,W},           {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W}, {2,LR,R}, {2,RR,R},           {1,LT,B}, {1,LB,B},           {1,RB,B}, {1,RT,B}                               };
M mTIEAx = { {5,ST,W},           {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,LR,R}, {3,RR,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,B}, {1,ST,W}, {1,RB,B}                                         };
M mTIEAG = {                     {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,W}, {1,ST,B}, {1,RB,W}                                         };
M mTIEBO = {           {5,KT,R}, {4,ST,B},           {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,KT,R},           {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,W}, {1,ST,B}, {1,RB,W}                                         };
M mTIEDE = { {5,ST,B},           {4,ST,B}, {4,KT,W}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,R}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,R}, {2,KT,R},                     {1,LT,R}, {1,LB,B},           {1,RB,B}, {1,RT,R}                               };
M mTIEFI = { {5,ST,W},           {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,KT,R},           {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,W},                               {1,RT,W}                               };
M mTIEIN = { {5,ST,W},           {4,ST,B}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,LS,R}, {3,RS,R}, {2,LT,B}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,B},                               {1,LT,W},                               {1,RT,W}                               };
M mTIEPH = {                     {4,ST,B}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,KT,R},           {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,W}, {1,LB,W},           {1,RB,W}, {1,RT,W}                               };
M mTIEPU = {                               {4,KT,R}, {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B},                               {0,SY,R} };
M mTIERE = {                                                   {3,LB,W}, {3,ST,B}, {3,RB,W},                               {2,LT,R}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,R},                               {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R}, {1,LS,R}, {1,RS,R}, {0,SY,R} };
M mTIEST = {                                                   {3,LB,W}, {3,ST,B}, {3,RB,W},                               {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W}, {2,LS,R}, {2,RS,R},           {1,LT,W}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,W}, {1,KT,R}                     };
M mUWING = {                     {4,ST,W},                     {3,LB,W}, {3,ST,W}, {3,RB,W},                               {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                                   {1,LB,B}, {1,ST,B}, {1,RB,B},                     {0,SY,R} };
M mVCX10 = {                     {4,ST,W}, {4,KT,R}, {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                                   {1,LB,W}, {1,ST,B}, {1,RB,W},                              };
M mVT49D = {                     {4,ST,W},           {3,LT,W}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,W},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                               {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R}                               };
M mYT13s = {                     {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,LS,R}, {3,RS,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                                   {1,LB,W}, {1,ST,B}, {1,RB,W}                               };
M mYT13b = {                     {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,LS,R}, {3,RS,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                                   {1,LB,W}, {1,ST,B}, {1,RB,W}                               };
M mYT240 = {                     {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,W},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LT,W}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,W}                     };
M mYV666 = {                     {4,ST,W},           {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,R}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,R},                                                   {1,LB,B}, {1,ST,B}, {1,RB,B},                     {0,SY,R} };
M mYWING = {                     {4,ST,R}, {4,KT,R}, {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                                   {1,LB,B}, {1,ST,B}, {1,RB,B}                               };
M mZ95HH = {                     {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,KT,R},           {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                                   {1,LB,W}, {1,ST,B}, {1,RB,W}                               };



std::vector<Ship> Ship::ships = {
  { Shp::Aggressor,      "Aggressor Assault Fighter",          "Aggressor",      BSz::Medium, mAGGRE },
  { Shp::AlphaClass,     "Alpha-class StarWing",               "StarWing",       BSz::Small,  mALPHA },
  { Shp::ARC170,         "ARC-170 Starfighter",                "ARC-170",        BSz::Medium, mARC17 },
  { Shp::AttackShuttle,  "Attack Shuttle",                     "Attack Shuttle", BSz::Small,  mATTSH },
  { Shp::Auzituck,       "Auzituck Gunship",                   "Auzituck",       BSz::Small,  mAUZIT },
  { Shp::BWing,          "A/SF-01 B-wing",                     "B-wing",         BSz::Small,  mBWING },
  { Shp::EscCraft,       "Escape Craft",                       "Escape Craft",   BSz::Small,  mESCCR },
  { Shp::EWing,          "E-wing",                             "E-wing",         BSz::Small,  mEWING },
  { Shp::FangFighter,    "Fang Fighter",                       "Fang Fighter",   BSz::Small,  mFANGF },
  { Shp::Firespray,      "Firespray-class Patrol Craft",       "Firespray",      BSz::Medium, mFIRES },
  { Shp::G1A,            "G-1A Starfighter",                   "G-1A",           BSz::Medium, mG1ASF },
  { Shp::HWK290,         "HWK-290 Light Freighter",            "HWK-290",        BSz::Small,  mHWK29 },
  { Shp::JM5K,           "JumpMaster 5000",                    "JumpMaster 5k",  BSz::Large,  mJUM5K },
  { Shp::Kihraxz,        "Kihraxz Fighter",                    "Kihraxz",        BSz::Small,  mKIHRA },
  { Shp::Kimogila,       "M12-L Kimogila Fighter",             "Kimogila",       BSz::Medium, mKIMOG },
  { Shp::KWing,          "BTL-S8 K-wing",                      "K-wing",         BSz::Medium, mKWING },
  { Shp::Lambda,         "Lambda-class T-4a Shuttle",          "Lambda-class",   BSz::Large,  mLAMBD },
  { Shp::LancerClass,    "Lancer-class Pursuit Craft",         "Lancer-class",   BSz::Large,  mLACPC },
  { Shp::M3A,            "M3-A Interceptor",                   "M3-A",           BSz::Small,  mM3AIN },
  { Shp::Quadjumper,     "Quadrijet Transfer Spacetug",        "Quadjumper",     BSz::Small,  mQUADJ },
  { Shp::RZ1AWing,       "RZ-1 A-wing",                        "RZ-1 A-wing",    BSz::Small,  mRZ1AW },
  { Shp::Scurrg,         "Scurrg H-6 Bomber",                  "Scurrg",         BSz::Medium, mSCURR },
  { Shp::Sheathipede,    "Sheathipede-class Shuttle",          "Sheathipede",    BSz::Small,  mSHEAT },
  { Shp::StarViper,      "StarViper-class Attack Platform",    "StarViper",      BSz::Small,  mSTARV },
  { Shp::T65XWing,       "T-65 X-wing",                        "T-65 X-wing",    BSz::Small,  mT65XW },
  { Shp::TIEAdvV1,       "TIE Advanced v1",                    "TIE Adv v1",     BSz::Small,  mTIEAv },
  { Shp::TIEAdvX1,       "TIE Advanced x1",                    "TIE Adv x1",     BSz::Small,  mTIEAx },
  { Shp::TIEAggressor,   "TIE/ag Aggressor",                   "TIE Aggressor",  BSz::Small,  mTIEAG },
  { Shp::TIEBomber,      "TIE/sa Bomber",                      "TIE Bomber",     BSz::Small,  mTIEBO },
  { Shp::TIEDefender,    "TIE/D Defender",                     "TIE Defender",   BSz::Small,  mTIEDE },
  { Shp::TIEFighter,     "TIE/ln Fighter",                     "TIE Fighter",    BSz::Small,  mTIEFI },
  { Shp::TIEInterceptor, "TIE Interceptor",                    "TIE Int",        BSz::Small,  mTIEIN },
  { Shp::TIEPhantom,     "TIE/ph Phantom",                     "TIE Phantom",    BSz::Small,  mTIEPH },
  { Shp::TIEPunisher,    "TIE/ca Punisher",                    "TIE Punisher",   BSz::Medium, mTIEPU },
  { Shp::TIEReaper,      "TIE Reaper",                         "Tie Reaper",     BSz::Medium, mTIERE },
  { Shp::TIEStriker,     "TIE/sk Striker",                     "TIE Striker",    BSz::Small,  mTIEST },
  { Shp::UWing,          "UT-60D U-wing",                      "U-Wing",         BSz::Medium, mUWING },
  { Shp::VCX100,         "VCX-100 Light Freighter",            "VCX-100",        BSz::Large,  mVCX10 },
  { Shp::VT49,           "VT-49 Decimator",                    "Decimator",      BSz::Large,  mVT49D },
  { Shp::YT1300scum,     "Customized YT-1300 Light Freighter", "YT-1300",        BSz::Large,  mYT13s },
  { Shp::YT1300reb,      "Modified YT-1300 Light Freighter",   "YT-1300",        BSz::Large,  mYT13b },
  { Shp::YT2400,         "YT-2400 Light Freighter",            "YT-2400",        BSz::Large,  mYT240 },
  { Shp::YV666,          "YV-666 Light Freighter",             "YV-666",         BSz::Large,  mYV666 },
  { Shp::YWing,          "BTL-A4 Y-wing",                      "BTL-A4 Y-Wing",  BSz::Small,  mYWING },
  { Shp::Z95,            "Z-95-AF4 Headhunter",                "Z-95",           BSz::Small,  mZ95HH },
};

}
