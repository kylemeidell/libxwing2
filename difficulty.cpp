#include "difficulty.h"

namespace libxwing2 {

// difficulty
std::vector<Difficulty> Difficulty::difficulties = {
  { Dif::Blue,  "Blue" },
  { Dif::White, "White" },
  { Dif::Red,   "Red" },
  { Dif::All,   "All" },
};

Dif operator++(Dif d) {
  switch(d) {
  case Dif::Blue:  return Dif::White;
  case Dif::White: return Dif::Red;
  case Dif::Red:   return Dif::Red;
  case Dif::All:   return Dif::All;
  }
}

Dif operator--(Dif d) {
  switch(d) {
  case Dif::Blue:  return Dif::Blue;
  case Dif::White: return Dif::Blue;
  case Dif::Red:   return Dif::White;
  case Dif::All:   return Dif::All;
  }
}

DifficultyNotFound::DifficultyNotFound(Dif d) : runtime_error("Difficulty not found (enum " + std::to_string((int)d) + ")") { }

Difficulty Difficulty::GetDifficulty(Dif typ) {
  for(Difficulty d : Difficulty::difficulties) {
    if(d.GetType() == typ) {
      return d;
    }
  }
  throw DifficultyNotFound(typ);
}

Dif         Difficulty::GetType()      const { return this->type; }
std::string Difficulty::GetName()      const { return this->name; }

Difficulty::Difficulty(Dif         t,
		       std::string n)
  : type(t), name(n) { }

}
