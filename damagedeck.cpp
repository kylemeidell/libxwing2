#include "damagedeck.h"

namespace libxwing2 {

// deck
DeckNotFound::DeckNotFound(Dck d) : runtime_error("Deck not found (enum " + std::to_string((int)d) + ")") { }
DeckNotFound::DeckNotFound(std::string d) : runtime_error("Deck not found '" + d + "'") { }

std::vector<Deck> Deck::deck = {
  { Dck::CoreSet, "core", "Core Set" },
};

Deck Deck::GetDeck(Dck type) {
  for(Deck d : Deck::deck) {
    if(d.GetType() == type) {
      return d;
    }
  }
  throw DeckNotFound(type);
}

Deck Deck::GetDeck(std::string deck) {
  for(Deck d : Deck::deck) {
    if(d.GetXws() == deck) {
      return d;
    }
  }
  throw DeckNotFound(deck);
}

std::vector<Deck> Deck::GetAllDecks() {
  return Deck::deck;
}

Dck         Deck::GetType() const { return this->type; }
std::string Deck::GetXws()  const { return this->xws;  }
std::string Deck::GetName() const { return this->name; }

Deck::Deck(Dck         t,
	   std::string x,
           std::string n)
  : type(t), xws(x), name(n) { }



// trait
TraitNotFound::TraitNotFound(Trt t) : runtime_error("Trait not found (enum " + std::to_string((int)t) + ")") { }

std::vector<Trait> Trait::trait = {
  { Trt::Pilot, "Pilot" },
  { Trt::Ship,  "Ship" },
};

Trait Trait::GetTrait(Trt type) {
  for(Trait t : Trait::trait) {
    if(t.GetType() == type) {
      return t;
    }
  }
  throw TraitNotFound(type);
}

Trt         Trait::GetType() const { return this->type; }
std::string Trait::GetName() const { return this->name; }

Trait::Trait(Trt         t,
             std::string n)
  : type(t), name(n) { }



// damage card
DamageCardNotFound::DamageCardNotFound(DmgCrd dc) : runtime_error("Card not found '" + std::to_string((int)dc.first) + "/" + Deck::GetDeck(dc.second).GetName() + "'") { }

std::vector<DamageCard> DamageCard::damageCards = {
  {  1, Dmg::PanickedPilot,          Dck::CoreSet, Trt::Pilot, 2, "Panicked Pilot",           "Gain 2 stress tokens.  Then repair this card." },
  {  2, Dmg::BlindedPilot,           Dck::CoreSet, Trt::Pilot, 2, "Blinded Pilot",            "While you perform an attack, you can modify your dice only by spending {FORCE} for their default effect.  Action: Repair this card." },
  {  3, Dmg::WoundedPilot,           Dck::CoreSet, Trt::Pilot, 2, "Wounded Pilot",            "After you perform an atcion, roll 1 attack die.  On a {HIT} or {CRIT} result, gain 1 stress token.  Action: Repair this card." },
  {  4, Dmg::StunnedPilot,           Dck::CoreSet, Trt::Pilot, 2, "Stunned Pilot",            "After you execute a maneuver, if you moved through or overlapped an obstacle, suffer 1 {HIT} damage." },
  {  5, Dmg::ConsoleFire,            Dck::CoreSet, Trt::Ship,  2, "Console Fire",             "Before you engage, roll 1 attack die.  On a {HIT} result, suffer 1 {HIT} damage.  Action: Repair this card." },
  {  6, Dmg::DamagedEngine,          Dck::CoreSet, Trt::Ship,  2, "Damaged Engine",           "Increase the difficulty of you turn maneuvers ({LTURN} and {RTURN})." },
  {  7, Dmg::WeaponsFailure,         Dck::CoreSet, Trt::Ship,  2, "Weapons Failure",          "While you perform an attack, roll 1 fewer attack die.  Action: Repair this card." },
  {  8, Dmg::HullBreach,             Dck::CoreSet, Trt::Ship,  2, "Hull Breach",              "Before you woudl suffer 1 or more {HIT} damage, sudder that much {CRIT} damage instead.  Action: Repair this card." },
  {  9, Dmg::StructuralDamage,       Dck::CoreSet, Trt::Ship,  2, "Structural Damage",        "While you defend, rikk 1 fewer defense die." },
  { 10, Dmg::DamagedSensorArray,     Dck::CoreSet, Trt::Ship,  2, "Damaged Sensor Array",     "You cannot perform any actions except the {FOCUS} action and actions from damage cards.  Action: Repair this card." },
  { 11, Dmg::LooseStabilizer,        Dck::CoreSet, Trt::Ship,  2, "Loost Stabilizer",         "After you execute a non-straight maneuver ({STRAIGHT}), suffer 1 {HIT} damage and repair this card.  Action: Repair this card." },
  { 12, Dmg::DisabledPowerRegulator, Dck::CoreSet, Trt::Ship,  2, "Disabled Power Regulator", "Before you engage, gain 1 ion token.  After you execute an ion maneuver, repair this card." },
  { 13, Dmg::FuelLeak,               Dck::CoreSet, Trt::Ship,  4, "Fuel Leak",                "After you suffer 1 {CRIT} damage, suffer 1 {HIT} damage and repair this card.  Action: Repair this card." },
  { 14, Dmg::DirectHit,              Dck::CoreSet, Trt::Ship,  5, "Direct Hit!",              "Suffer 1 {HIT} damage.  Then repair this card." },
};

DamageCard DamageCard::GetDamageCard(DmgCrd dc) {
  for(DamageCard d : DamageCard::damageCards) {
    if((d.GetDamage() == dc.first) && (d.GetDeck().GetType() == dc.second)) {
      return d;
    }
  }
  throw DamageCardNotFound(dc);
}

std::vector<DamageCard> DamageCard::GetAllDamageCards(Dck d) {
  std::vector<DamageCard> ret;
  for(DamageCard dc : DamageCard::damageCards) {
    if(dc.GetDeck().GetType() == d) {
      ret.push_back(dc);
    }
  }
  return ret;
}

uint8_t     DamageCard::GetNumber() const { return this->number; }
Dmg         DamageCard::GetDamage() const { return this->damage; }
Deck        DamageCard::GetDeck()   const { return Deck::GetDeck(this->deck); }
Trait       DamageCard::GetTrait()  const { return Trait::GetTrait(this->trait); }
uint8_t     DamageCard::GetCount()  const { return this->count; }
std::string DamageCard::GetName()   const { return this->name; }
std::string DamageCard::GetText()   const { return this->text; }

DamageCard::DamageCard(uint8_t      num,
		       Dmg          dmg,
                       Dck          dck,
                       Trt          trt,
                       uint8_t      cou,
                       std::string  nam,
                       std::string  txt)
  : number(num), damage(dmg), deck(dck), trait(trt), count(cou), name(nam), text(txt) { }

}
