#pragma once
#include <stdint.h>

class Chargeable {
 public:
  Chargeable(int8_t cap, bool rech);
  int8_t GetCapacity();
  bool   IsRechargeable();

 private:
  int8_t capacity;
  bool isRechargeable;
};
