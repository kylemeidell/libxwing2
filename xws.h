#pragma once
#include "condition.h"
#include "pilot.h"
#include "ship.h"

namespace libxwing2 {
namespace xws {

namespace faction {
  class FactionNotFound : public std::runtime_error {
  public:
    FactionNotFound(Fac         f);
    FactionNotFound(std::string x);
  };

  Fac         GetFaction(std::string x);
  std::string GetFaction(Fac         f);
  std::vector<Fac> GetMissing();
}



namespace ship {
  class ShipNotFound : public std::runtime_error {
  public:
    ShipNotFound(Shp         s);
    ShipNotFound(std::string x);
  };
  Shp         GetShip(std::string x);
  std::string GetShip(Shp         s);
  std::vector<Shp> GetMissing();
}



namespace pilot {
  struct xwsData {
    std::string pilot;
    std::string faction;
    std::string ship;
  };

  class PilotNotFound : public std::runtime_error {
  public:
    PilotNotFound(Plt     s);
    PilotNotFound(xwsData x);
  };

  Plt     GetPilot(xwsData x);
  xwsData GetPilot(Plt     p);
  std::vector<Plt> GetMissing();
}



namespace upgradetype {
  class UpgradeTypeNotFound : public std::runtime_error {
  public:
    UpgradeTypeNotFound(UpT         u);
    UpgradeTypeNotFound(std::string x);
  };

  UpT         GetUpgradeType(std::string x);
  std::string GetUpgradeType(UpT         u);
  std::vector<UpT> GetMissing();
}



namespace upgrade {
  struct xwsData {
    std::string type;
    std::string name;
  };

  class UpgradeNotFound : public std::runtime_error {
  public:
    UpgradeNotFound(Upg     u);
    UpgradeNotFound(xwsData x);
  };


  Upg     GetUpgrade(xwsData x);
  xwsData GetUpgrade(Upg     u);
  std::vector<Upg> GetMissing();
}



namespace condition {
  class ConditionNotFound : public std::runtime_error {
  public:
    ConditionNotFound(Cnd         c);
    ConditionNotFound(std::string x);
  };

  Cnd         GetCondition(std::string x);
  std::string GetCondition(Cnd         c);
  std::vector<Cnd> GetMissing();
}

}}
