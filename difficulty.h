#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

// difficulty
enum class Dif {
  Blue  = 0x01,
  White = 0x02,
  Red   = 0x04,
  All   = 0x07
};

Dif operator++(Dif);
Dif operator--(Dif);

class DifficultyNotFound : public std::runtime_error {
 public:
  DifficultyNotFound(Dif d);
};

class Difficulty {
 public:
  static Difficulty GetDifficulty(Dif d);
  Dif         GetType()      const;
  std::string GetName()      const;

 private:
  Dif type;
  std::string name;

  static std::vector<Difficulty> difficulties;

  Difficulty(Dif         f,
	     std::string n);
};

}
