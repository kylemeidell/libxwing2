#include "release.h"

namespace libxwing2 {

// ToKen
#define TK(t)   std::make_shared<Tokens>(Tok::t,1)
// TokenS
#define TS(t,c) std::make_shared<Tokens>(Tok::t,c)
// LocK
#define LK(n)   std::make_shared<LkToken>(n)
// ID
#define ID(n)   std::make_shared<IDTokens>(n)

std::vector<Release> Release::releases = {

// [Info]
// Rel  - enum key
// SKU  - product SKU
// ISBN - product ISBN
// Name - expansion name
// Type - expansion type (wave#, coreset, aces, epic)

// [Dates] (year, month, date)
// Announced - the date this expansion was announced
// Released  - the date this expansion was released

// [URLs]
// AnnouncementURL - url to expansion announcement article
// PreviewURLS     - url(s) to expansion preview article(s)
// ReleaseURL      - url to expansion release article

// [Items]

// [Ships] (ships included in the expansion)
// Ship - the ship
// Desc - a description (for alternate paint jobs)

// [Pilots] (pilots included in the expansion)
// XwsName - pilot name per xws format
// Faction - pilot faction
// Ship    - pilot ship

// [Upgrades]
// Type    - upgrade type
// XwsName - upgrade name per xws format

// [Conditions] (condition cards included in the expansion)
// XwsName - xws-formatted name of the condition card

// [Tokens] (tokens included in the expansion)
// TK(type)        - single token
// TS(type, count) - multiple tokens
// ID(num)         - id token set (3 of number 'num')
// TL(a,b)         - target lock set (red and blue with letters 'a' on one side and 'b' on the other)

//{ Rel,     SKU,     ISBN,            Name,                                           Type,               Announced,    Released,
//                                                                                                { AnnouncementURL,
//                                                                                                 {PreviewURLs},
//                                                                                                  ReleaseURL},
//                                                                                                {{Ship, Desc}},
//                                                                                                 {{XwsName, Faction, Ship}},
//                                                                                                 {{Type, XwsName}},
//                                                                                                 {XwsName},
//                                                                                                 {TK(type), TS(type,4), ID(1), TL("A","B")}
//                                                                                                },

  { Rel::SWX74, "SWX74", "841333105303", "Saw's Renegades Expansion Pack", RelGroup::FeWave14,
    {2018, 2,13}, {2018, 6,21},
    {"https://www.fantasyflightgames.com/en/news/2018/2/13/save-the-dream/",
     {"https://www.fantasyflightgames.com/en/news/2018/6/5/extreme-action/"},
     "https://www.fantasyflightgames.com/en/news/2018/6/21/built-on-hope/"},
    { {Shp::T65XWing,"Blank & White"}, {Shp::UWing,"Blank & White"} },
    { Plt::KSperado, Plt::LTenza, Plt::ETwoTubes, Plt::CAZealot, Plt::CAZealot, Plt::CAZealot, Plt::SGerrera, Plt::MYarro, Plt::BTwoTubes, Plt::PartRenegade },
    { Upg::R3Astro, Upg::R4Astro, Upg::PvtWing, Upg::SFoils, Upg::MYarro, Upg::SGerrera, Upg::DeadmansSw, Upg::DeadmansSw, Upg::AdvSensors, Upg::TrickSh, Upg::TrickSh, Upg::ProtTrp },
    {}, // condition
    {}
  },

  { Rel::SWX75, "SWX75", "841333105310", "TIE Reaper Expansion Pack", RelGroup::FeWave14,
    {2018, 2,13}, {2018, 6,21},
    {"https://www.fantasyflightgames.com/en/news/2018/2/13/save-the-dream/",
     {"https://www.fantasyflightgames.com/en/news/2018/6/15/transporting-terror/"},
     "https://www.fantasyflightgames.com/en/news/2018/6/21/built-on-hope/"},
    { {Shp::TIEReaper,""} },
    { Plt::MajVermeil, Plt::CaptFeroph, Plt::Vizier, Plt::ScarifBsPlt },
    { Upg::DeathTr, Upg::DirKrennic, Upg::ISBSlicer, Upg::ISBSlicer, Upg::TactOff, Upg::TactOff, Upg::Juke, Upg::Juke, Upg::SwarmTac, Upg::SwarmTac },
    { Cnd::OptProt },
    {}
  },

  { Rel::SWZ01, "SWZ01", "841333105587", "X-Wing Second Edition", RelGroup::CoreSet,
    {2018, 5, 1}, {2018, 9,13},
    {"https://www.fantasyflightgames.com/en/news/2018/5/1/x-wing-second-edition/",
     {},
     ""},
    { {Shp::T65XWing,""}, {Shp::TIEFighter,""}, {Shp::TIEFighter,""} },
    {
      Plt::LSkywalker, Plt::JPorkins, Plt::RedSqVet, Plt::BlueSqEsc,
      Plt::IVersio, Plt::BlackSqAce, Plt::BlackSqAce, Plt::VRudor, Plt::NightBeast, Plt::ObsidianSqPlt, Plt::ObsidianSqPlt, Plt::AcademyPlt, Plt::AcademyPlt
    },
    {
      Upg::R2Astro, Upg::R2D2, Upg::R3Astro, Upg::R5Astro, Upg::R5D8,
      Upg::SFoils,
      Upg::HeightPerc, Upg::HeightPerc, Upg::InstAim, Upg::Sense, Upg::Sense, Upg::SNReflex, Upg::SNReflex,
      Upg::Afterburn, Upg::HullUpg, Upg::ShieldUpg,
      Upg::Elusive, Upg::Outmaneuv, Upg::Predator,
      Upg::ProtTrp },
    { },
    {
      TS(Crit,3), TK(Disarm), TS(Evade,3), TS(Focus,4), TS(ForceCharge,2), ID(1), ID(2), ID(3), ID(4), ID(5), ID(6), TS(Ion,3), LK(1), LK(2), LK(3), LK(4), LK(5), LK(6), TS(Shield,4), TS(StandardCharge,6), TS(Stress,5),
      TS(Asteroid,3), TS(DebrisCloud,3),
      TK(FirstPlayerMarker), TS(HyperspaceMarker,2)
    }
  },

  { Rel::SWZ04, "SWZ04", "", "Lando's Millennium Falcon Expansion Pack", RelGroup::Wave1,
    {2018, 6, 7}, {2018, 9,13},
    {"https://www.fantasyflightgames.com/en/news/2018/6/7/fast-enough/",
     {"https://www.fantasyflightgames.com/en/news/2018/6/25/got-it-where-it-counts/"},
     ""},
    { {Shp::YT1300scum,""}, {Shp::EscCraft,""} },
    { Plt::HSoloScum, Plt::LCalrissianScum, Plt::L337C, Plt::ORPioneer, Plt::L337E, Plt::AutoPltDrone },
    { /*Upg::*/ },
    { /*Cnd::*/ },
    {}
  },

  { Rel::SWZ06, "SWZ06", "841333105631", "Rebel Alliance Conversion Kit", RelGroup::ConvKit,
    {2018, 5, 1}, {2018, 9,13},
    {"https://www.fantasyflightgames.com/en/news/2018/5/1/all-wings-report-in-1/",
     {},
     ""},
    { },
    {
      //ARC170
      Plt::NWexley_ARC, Plt::GDreis_ARC, Plt::SBey, Plt::Ibtisam,
      // Attack Shuttle
      Plt::HSyndulla_AS, Plt::EBridger_AS, Plt::SWren_AS, Plt::ZOrrelios_AS,
      // Auzituck
      Plt::Wullffwarro, Plt::Lowhhrick, Plt::KashyyykDef, Plt::KashyyykDef,
      // B-wing
      Plt::BStramm, Plt::TNumb, Plt::BladeSqVet, Plt::BladeSqVet, Plt::BlueSqPlt, Plt::BlueSqPlt,
      // E-wing
      Plt::CHorn, Plt::GDarklighter, Plt::RogueSqEsc, Plt::RogueSqEsc, Plt::KnaveSqEsc, Plt::KnaveSqEsc,
      // HWK-290
      Plt::JOrs, Plt::RGarnet, Plt::KKatarn, Plt::RebelScout, Plt::RebelScout,
      // K-wing
      Plt::MDoni, Plt::ETuketu, Plt::WardenSqPlt, Plt::WardenSqPlt,
      // RZ1 A-wing
      Plt::JFarrell, Plt::ACrynyd, Plt::GreenSqPlt, Plt::GreenSqPlt, Plt::PhoenixSqPlt, Plt::PhoenixSqPlt,
      // Sheathipede
      Plt::FRau_Sh, Plt::EBridger_Sh, Plt::ZOrrelios_Sh, Plt::AP5,
      // T65 X-wing
      Plt::WAntilles, Plt::BDarklighter, Plt::GDreis_XW, Plt::BlueSqEsc, Plt::BlueSqEsc, Plt::RedSqVet, Plt::RedSqVet,
      // TIE Fighter
      Plt::EBridger_TF, Plt::SWren_TF, Plt::ZOrrelios_TF, Plt::CaptRex,
      // U-wing
      Plt::BRook, Plt::CAndor, Plt::HTobber, Plt::BlueSqScout,
      // VCX-100
      Plt::HSyndulla_VCX, Plt::KJarrus, Plt::Chopper, Plt::LothalRebel,
      // YT-1300
      Plt::HSoloReb, Plt::LCalrissianReb, Plt::Chewbacca, Plt::OuterRimSmug,
      // YT-2400
      Plt::DRendar, Plt::Leebo, Plt::WildSpaceFrin, Plt::WildSpaceFrin,
      // Y-wing
      Plt::DVander, Plt::HSalm, Plt::GoldSqVet, Plt::GoldSqVet, Plt::GraySqBomber, Plt::GraySqBomber,
      // Z-95
      Plt::ACracken, Plt::LtBlount, Plt::TalaSqPlt, Plt::TalaSqPlt, Plt::TalaSqPlt, Plt::BanditSqPlt, Plt::BanditSqPlt, Plt::BanditSqPlt
    },
    {
      // Astromech
      Upg::ChopperA, Upg::R2Astro, Upg::R2Astro, Upg::R3Astro, Upg::R3Astro, Upg::R4Astro, Upg::R4Astro, Upg::R5Astro, Upg::R5Astro,
      // Bomb
      Upg::Bomblet, Upg::ConnerNet, Upg::ConnerNet, Upg::ProtonBmb, Upg::ProtonBmb, Upg::ProxMine, Upg::ProxMine, Upg::SeismicCh, Upg::SeismicCh,
      // Cannon
      Upg::HLC, Upg::HLC, Upg::IonCan, Upg::IonCan, Upg::JamBeam, Upg::JamBeam, Upg::TracBeam, Upg::TracBeam,
      // Configuration
      Upg::PvtWing, Upg::PvtWing, Upg::SFoils, Upg::SFoils,
      // Crew
      Upg::BMalbus, Upg::C3PO, Upg::CAndor, Upg::ChewieReb, Upg::ChopperC, Upg::FLSlicer, Upg::FLSlicer, Upg::Gonk, Upg::Gonk, Upg::HSyndulla, Upg::Informant, Upg::JErso, Upg::KJarrus, Upg::LCalrissianReb, Upg::LOrgana, Upg::NNunb, Upg::NovTech, Upg::NovTech, Upg::PerCPilot, Upg::PerCPilot, Upg::R2D2C, Upg::SWren, Upg::SeasonNav, Upg::SeasonNav, Upg::TactOff, Upg::TactOff, Upg::ZOrrelios,
      // Gunner
      Upg::Bistan, Upg::EBridger, Upg::HSoloG, Upg::HsGunner, Upg::HsGunner, Upg::LSkywalkerG, Upg::SkBombard, Upg::SkBombard, Upg::VetTailGun, Upg::VetTailGun, Upg::VetTurret, Upg::VetTurret,
      // Illicit
      Upg::CloakDev, Upg::ContraCyb, Upg::ContraCyb, Upg::DeadmansSw, Upg::DeadmansSw, Upg::FeedbackAr, Upg::FeedbackAr, Upg::InertDamp, Upg::InertDamp, Upg::RigCargoCh, Upg::RigCargoCh,
      // Modification
      Upg::AblatPlat, Upg::AblatPlat, Upg::AdvSLAM, Upg::AdvSLAM, Upg::ElectBaff, Upg::ElectBaff, Upg::EngUpg, Upg::EngUpg, Upg::HullUpg, Upg::HullUpg, Upg::MuniFailSa, Upg::MuniFailSa, Upg::ShieldUpg, Upg::ShieldUpg, Upg::StaDcVanes, Upg::StaDcVanes, Upg::StealthDev, Upg::StealthDev, Upg::TactScramb, Upg::TactScramb,
      // Missile
      Upg::ClustMsl, Upg::ClustMsl, Upg::ConcusMsl, Upg::ConcusMsl, Upg::HomingMsl, Upg::HomingMsl, Upg::IonMsl, Upg::IonMsl, Upg::PRockets, Upg::PRockets,
      // System
      Upg::AdvSensors, Upg::AdvSensors, Upg::CollisDet, Upg::CollisDet, Upg::FCS, Upg::FCS,
      // Talent
      Upg::CrackShot, Upg::CrackShot, Upg::Daredevil, Upg::Daredevil, Upg::DebGambit, Upg::DebGambit, Upg::Elusive, Upg::Elusive, Upg::ExpHan, Upg::ExpHan, Upg::Intimidat, Upg::Intimidat, Upg::Juke, Upg::Juke, Upg::LoneWolf, Upg::Marksman, Upg::Marksman, Upg::Outmaneuv, Upg::Outmaneuv, Upg::Predator, Upg::Predator, Upg::SatSalvo, Upg::SatSalvo, Upg::Selfless, Upg::Selfless, Upg::Selfless, Upg::SquadLdr, Upg::SwarmTac, Upg::SwarmTac, Upg::TrickSh, Upg::TrickSh,
      // Title
      Upg::Ghost, Upg::MilFalcon, Upg::MoldyCrow, Upg::Outrider, Upg::Phantom,
      // Torpedo
      Upg::AdvProtTrp, Upg::AdvProtTrp, Upg::IonTrp, Upg::IonTrp, Upg::ProtTrp, Upg::ProtTrp,
      // Turret
      Upg::DorsalTrt, Upg::DorsalTrt, Upg::IonCanTrt, Upg::IonCanTrt
    },
    { Cnd::LstnDev, Cnd::SupFire },
    {
      TS(Calculate,3), TK(Cloak), TS(Disarm,2), TS(ForceCharge,4), TS(Jam,2), TS(Reinforce,2), TS(StandardCharge,5), TS(Tractor,2),
      TK(ListeningDevice), TK(SuppressiveFire)
    }
  },

  { Rel::SWZ07, "SWZ07", "841333105648", "Galactic Empire Conversion Kit", RelGroup::ConvKit,
    {2018, 5, 1}, {2018, 9,13},
    {"https://www.fantasyflightgames.com/en/news/2018/5/1/crush-the-rebellion-1//",
     {},
     ""},
    { },
    {
      // Decimator
      Plt::RAdmChiraneau, Plt::CaptOicunn, Plt::PatrolLdr, Plt::PatrolLdr,
      // Gunboat
      Plt::MajVynder, Plt::LtKarsabi, Plt::RhoSqPlt, Plt::RhoSqPlt, Plt::NuSqPlt, Plt::NuSqPlt,
      // Lambda
      Plt::CaptKagi, Plt::ColJendon, Plt::LtSai, Plt::OmicronGrpPlt,
      // TIE Advanced v1
      Plt::GrandInq, Plt::SeventhSister, Plt::BaronOfEmp, Plt::BaronOfEmp, Plt::BaronOfEmp, Plt::Inq, Plt::Inq, Plt::Inq,
      // TIE Advanced x1
      Plt::DVader, Plt::MStele, Plt::VFoslo, Plt::ZStrom, Plt::StormSqAce, Plt::StormSqAce, Plt::TempestSqPlt, Plt::TempestSqPlt,
      // TIE Aggressor
      Plt::LtKestal, Plt::OnyxSqScout, Plt::DblEdge, Plt::DblEdge, Plt::SienSpecialist, Plt::SienSpecialist,
      // TIE Bomber
      Plt::TBren,  Plt::CaptJonus, Plt::MajRhymer, Plt::GammaSqAce, Plt::GammaSqAce, Plt::GammaSqAce, Plt::Deathfire, Plt::ScimitarSqPlt, Plt::ScimitarSqPlt, Plt::ScimitarSqPlt,
      // TIE Defender
      Plt::RBrath, Plt::ColVessery, Plt::CountessRyad, Plt::OnyxSqAce, Plt::OnyxSqAce, Plt::DeltaSqPlt, Plt::DeltaSqPlt,
      // TIE Fighter
      Plt::Howlrunner, Plt::MMithel, Plt::SSkutu, Plt::DMeeko, Plt::GHask, Plt::SMarana, Plt::BlackSqAce, Plt::BlackSqAce, Plt::BlackSqAce, Plt::BlackSqAce, Plt::ObsidianSqPlt, Plt::ObsidianSqPlt, Plt::ObsidianSqPlt, Plt::ObsidianSqPlt, Plt::AcademyPlt, Plt::AcademyPlt, Plt::AcademyPlt, Plt::AcademyPlt, Plt::Wampa,
      // TIE Interceptor
      Plt::SFel, Plt::SaberSqAce, Plt::SaberSqAce, Plt::TPhennir, Plt::AlphaSqPlt, Plt::AlphaSqPlt,
      // TIE Phantom
      Plt::Whisper, Plt::Echo, Plt::SigmaSqAce, Plt::SigmaSqAce, Plt::ImdaarTestPlt, Plt::ImdaarTestPlt,
      // TIE Punisher
      Plt::Redline, Plt::Deathrain, Plt::CutlassSqPlt, Plt::CutlassSqPlt,
      // TIE Striker
      Plt::Duchess, Plt::Countdown, Plt::PureSabacc, Plt::BlackSqScout, Plt::BlackSqScout, Plt::BlackSqScout, Plt::PlanetarySent, Plt::PlanetarySent, Plt::PlanetarySent
    },
    {
      // Bomb
      Upg::Bomblet, Upg::ConnerNet, Upg::ConnerNet, Upg::ConnerNet, Upg::ProtonBmb, Upg::ProtonBmb, Upg::ProtonBmb, Upg::ProxMine, Upg::ProxMine, Upg::ProxMine, Upg::SeismicCh, Upg::SeismicCh, Upg::SeismicCh,
      // Configuration
      Upg::Os1ArsenalLdt, Upg::Os1ArsenalLdt, Upg::Os1ArsenalLdt, Upg::Xg1AssaultCfg, Upg::Xg1AssaultCfg, Upg::Xg1AssaultCfg,
      // Crew
      Upg::AdmSloane, Upg::AgentKallus, Upg::CRee, Upg::DVader, Upg::EmpPalp, Upg::FLSlicer, Upg::FLSlicer, Upg::Gonk, Upg::Gonk, Upg::GrandInq, Upg::GrandMoffTarkin, Upg::Informant, Upg::MinTua, Upg::MoffJerjerrod, Upg::NovTech, Upg::NovTech, Upg::PerCPilot, Upg::PerCPilot, Upg::SeasonNav, Upg::SeasonNav, Upg::SeventhSister, Upg::TactOff, Upg::TactOff,
      // Gunner
      Upg::FifthBrother, Upg::FifthBrother, Upg::HsGunner, Upg::HsGunner, Upg::SkBombard, Upg::SkBombard, Upg::VetTurret, Upg::VetTurret,
      // Missile
      Upg::BarRockets, Upg::BarRockets, Upg::BarRockets, Upg::ClustMsl, Upg::ClustMsl, Upg::ClustMsl, Upg::ConcusMsl, Upg::ConcusMsl, Upg::ConcusMsl, Upg::HomingMsl, Upg::HomingMsl, Upg::HomingMsl, Upg::IonMsl, Upg::IonMsl, Upg::IonMsl, Upg::PRockets, Upg::PRockets, Upg::PRockets,
      // Modification
      Upg::AblatPlat, Upg::AblatPlat, Upg::AdvSLAM, Upg::AdvSLAM, Upg::ElectBaff, Upg::ElectBaff, Upg::HullUpg, Upg::HullUpg, Upg::MuniFailSa, Upg::MuniFailSa, Upg::StaDcVanes, Upg::StaDcVanes, Upg::ShieldUpg, Upg::ShieldUpg, Upg::StealthDev, Upg::StealthDev, Upg::TactScramb, Upg::TactScramb,
      // System
      Upg::AdvSensors, Upg::AdvSensors, Upg::CollisDet, Upg::CollisDet, Upg::FCS, Upg::FCS, Upg::TrajSim, Upg::TrajSim,
      // Talent
      Upg::CrackShot, Upg::CrackShot, Upg::CrackShot, Upg::DebGambit, Upg::DebGambit, Upg::Elusive, Upg::Elusive, Upg::Intimidat, Upg::Intimidat, Upg::Intimidat, Upg::Juke, Upg::Juke, Upg::Juke, Upg::LoneWolf, Upg::Outmaneuv, Upg::Outmaneuv, Upg::Outmaneuv, Upg::Predator, Upg::Predator, Upg::Predator, Upg::Ruthless, Upg::Ruthless, Upg::Ruthless, Upg::SatSalvo, Upg::SatSalvo, Upg::SquadLdr, Upg::SwarmTac, Upg::SwarmTac, Upg::TrickSh, Upg::TrickSh, Upg::TrickSh,
      // Title
      Upg::Dauntless, Upg::ST321,
      // Torpedo
      Upg::AdvProtTrp, Upg::AdvProtTrp, Upg::AdvProtTrp, Upg::IonTrp, Upg::IonTrp, Upg::IonTrp, Upg::ProtTrp, Upg::ProtTrp, Upg::ProtTrp,
      // Turret
      Upg::DorsalTrt, Upg::DorsalTrt, Upg::IonCanTrt, Upg::IonCanTrt
    },
    { Cnd::Hunted, Cnd::LstnDev },
    {
       TK(Calculate), TS(Cloak,3), TS(Disarm,3), TS(Focus,4), TS(ForceCharge,6), TS(Jam,3), TS(Reinforce,2), TS(StandardCharge,10), TS(Stress,4), TS(Tractor,3),
       TK(Hunted), TK(ListeningDevice)
    }
  },

  { Rel::SWZ08, "SWZ08", "841333105655", "Scum and Villainy Conversion Kit", RelGroup::ConvKit,
    {2018, 5, 1}, {2018, 9,13},
    {"https://www.fantasyflightgames.com/en/news/2018/5/1/become-infamous/",
     {},
     ""},
    { },
    {
      // Aggressor
      Plt::IG88A, Plt::IG88B, Plt::IG88C, Plt::IG88D,
      // Fang Fighter
      Plt::FRau_FF, Plt::OTeroch, Plt::JRekkoff, Plt::KSolus, Plt::SkullSqPlt, Plt::SkullSqPlt, Plt::ZealousRecruit, Plt::ZealousRecruit, Plt::ZealousRecruit,
      // Firespray
      Plt::BFett, Plt::EAzzameen, Plt::KScarlet, Plt::KFrost, Plt::KTrelix, Plt::BountyHunter, Plt::BountyHunter,
      // G1-A
      Plt::FourLOM, Plt::Zuckuss, Plt::GandFindsman, Plt::GandFindsman,
      // HWK-290
      Plt::DBonearm, Plt::PGodalhi, Plt::TMux, Plt::SpiceRunner, Plt::SpiceRunner,
      // Jumpmaster 5000
      Plt::Dengar, Plt::TTrevura, Plt::Manaroo, Plt::ContractedSc,
      // Kihraxz
      Plt::TCobra, Plt::Graz, Plt::VHel, Plt::BlackSunAce, Plt::BlackSunAce, Plt::BlackSunAce, Plt::CaptJostero, Plt::CartMarauder, Plt::CartMarauder, Plt::CartMarauder,
      // Kimogila
      Plt::TKulda, Plt::DOberos_KG, Plt::CartExecution, Plt::CartExecution,
      // Lancer-class
      Plt::KOnyo, Plt::AVentress, Plt::SWren_LC, Plt::ShadowportHun,
      // M3A
      Plt::Serissu, Plt::GRed, Plt::LAshera, Plt::QJast, Plt::TansariiPtVet, Plt::TansariiPtVet, Plt::TansariiPtVet, Plt::TansariiPtVet, Plt::Inaldra, Plt::CartelSpacer, Plt::CartelSpacer, Plt::CartelSpacer, Plt::CartelSpacer, Plt::SBounder,
      // Quadjumper
      Plt::ConstZuvio, Plt::SPlank, Plt::UPlutt, Plt::JGunrunner, Plt::JGunrunner, Plt::JGunrunner,
      // Scurrg
      Plt::CaptNym, Plt::SSixxa, Plt::LRevenant, Plt::LRevenant,
      // Starviper
      Plt::Guri, Plt::DOberos_SV, Plt::PXizor, Plt::BSAssassin, Plt::BSAssassin, Plt::BSEnforcer, Plt::BSEnforcer,
      // YV-666
      Plt::Bossk, Plt::MEval, Plt::LRazzi, Plt::TSlaver,
      // Y-wing
      Plt::Kavil, Plt::DRenthal, Plt::HiredGun, Plt::HiredGun, Plt::CrymorahGoon, Plt::CrymorahGoon,
      // Z-95
      Plt::NSuhlak, Plt::BlackSunSoldier, Plt::BlackSunSoldier, Plt::BlackSunSoldier, Plt::KLeeachos, Plt::BinayrePirate, Plt::BinayrePirate, Plt::BinayrePirate, Plt::NashtahPup,
     },
    {
      // Astromech
      Upg::Genius, Upg::R2Astro, Upg::R2Astro, Upg::R3Astro, Upg::R3Astro, Upg::R4Astro, Upg::R4Astro, Upg::R5Astro, Upg::R5Astro, Upg::R5P8, Upg::R5TK,
      // Bomb
      Upg::Bomblet, Upg::ConnerNet, Upg::ConnerNet, Upg::ProtonBmb, Upg::ProtonBmb, Upg::ProxMine, Upg::ProxMine, Upg::SeismicCh, Upg::SeismicCh,
      // Cannon
      Upg::HLC, Upg::HLC, Upg::IonCan, Upg::IonCan, Upg::JamBeam, Upg::JamBeam, Upg::TracBeam, Upg::TracBeam,
      // Crew
      Upg::BFett, Upg::CBane, Upg::CVizago, Upg::FourLOM, Upg::FLSlicer, Upg::FLSlicer, Upg::Gonk, Upg::Gonk, Upg::IG88D, Upg::Informant, Upg::Jabba, Upg::KOnyo, Upg::LRazzi, Upg::Maul, Upg::NovTech, Upg::NovTech, Upg::PerCPilot, Upg::PerCPilot, Upg::SeasonNav, Upg::SeasonNav, Upg::TactOff, Upg::TactOff, Upg::TripleZero, Upg::UPlutt, Upg::Zuckuss,
      // Gunner
      Upg::Bossk, Upg::BT1, Upg::Dengar, Upg::Greedo, Upg::HsGunner, Upg::HsGunner, Upg::SkBombard, Upg::SkBombard, Upg::VetTailGun, Upg::VetTailGun, Upg::VetTurret, Upg::VetTurret,
      // Illicit
      Upg::CloakDev, Upg::ContraCyb, Upg::ContraCyb, Upg::DeadmansSw, Upg::DeadmansSw, Upg::DeadmansSw, Upg::FeedbackAr, Upg::FeedbackAr, Upg::FeedbackAr, Upg::InertDamp, Upg::InertDamp, Upg::RigCargoCh, Upg::RigCargoCh,
      // Missile
      Upg::ClustMsl, Upg::ClustMsl, Upg::ConcusMsl, Upg::ConcusMsl, Upg::HomingMsl, Upg::HomingMsl, Upg::IonMsl, Upg::IonMsl, Upg::PRockets, Upg::PRockets,
      // Modification
      Upg::AblatPlat, Upg::AblatPlat, Upg::ElectBaff, Upg::ElectBaff, Upg::EngUpg, Upg::EngUpg, Upg::HullUpg, Upg::HullUpg, Upg::MuniFailSa, Upg::MuniFailSa, Upg::ShieldUpg, Upg::ShieldUpg, Upg::StaDcVanes, Upg::StaDcVanes, Upg::StealthDev, Upg::StealthDev, Upg::TactScramb, Upg::TactScramb,
      // System
      Upg::AdvSensors, Upg::AdvSensors, Upg::CollisDet, Upg::CollisDet, Upg::FCS, Upg::FCS, Upg::TrajSim, Upg::TrajSim,
      // Talent
      Upg::CrackShot, Upg::CrackShot, Upg::Daredevil, Upg::Daredevil, Upg::DebGambit, Upg::DebGambit, Upg::Elusive, Upg::Elusive, Upg::ExpHan, Upg::ExpHan, Upg::Fearless, Upg::Fearless, Upg::Intimidat, Upg::Intimidat, Upg::Intimidat, Upg::Juke, Upg::Juke, Upg::LoneWolf, Upg::Marksman, Upg::Marksman, Upg::Outmaneuv, Upg::Outmaneuv, Upg::Predator, Upg::Predator, Upg::SatSalvo, Upg::SatSalvo, Upg::SquadLdr, Upg::SwarmTac, Upg::SwarmTac, Upg::TrickSh, Upg::TrickSh,
      // Title
      Upg::Andrasta, Upg::Havoc, Upg::IG2000, Upg::IG2000, Upg::HoundsTooth, Upg::Marauder, Upg::MistHunter, Upg::PunishingOne, Upg::ShadowCaster, Upg::SlaveI, Upg::Virago,
      // Torpedo
      Upg::AdvProtTrp, Upg::AdvProtTrp, Upg::IonTrp, Upg::IonTrp, Upg::ProtTrp, Upg::ProtTrp,
      // Turret
      Upg::DorsalTrt, Upg::DorsalTrt, Upg::IonCanTrt, Upg::IonCanTrt,
    },
    { Cnd::LstnDev },
    {
      TS(Calculate,3), TK(Cloak), TS(Disarm,3), TS(Focus,3), TS(ForceCharge,3), TS(Ion,2), TS(Jam,3), TS(Reinforce,2), TS(StandardCharge,5), TS(Stress,2), TS(Tractor,3),
      TK(ListeningDevice)
    }
  },

  { Rel::SWZ12, "SWZ12", "841333106041", "T-65 X-Wing Expansion Pack", RelGroup::Wave1,
    {2018, 5, 1}, {2018, 9,13},
    {"https://www.fantasyflightgames.com/en/news/2018/6/11/symbol-of-the-rebellion/",
     {"https://www.fantasyflightgames.com/en/news/2018/6/11/symbol-of-the-rebellion/"},
     ""},
    {{Shp::T65XWing, ""}},
    { Plt::WAntilles, Plt::BDarklighter, Plt::TKyrell, Plt::GDreis_XW,  },
    { Upg::SFoils },
    { /*Cnd::*/ },
    {}
  },

  { Rel::SWZ13, "SWZ13", "841333106058", "BTL-A4 Y-Wing Expansion Pack", RelGroup::Wave1,
    {2018, 5, 1}, {2018, 9,13},
    {"https://www.fantasyflightgames.com/en/news/2018/5/1/all-wings-report-in-1/",
     {"https://www.fantasyflightgames.com/en/news/2018/6/18/begin-attack-run/"},
     ""},
    {{Shp::YWing, ""}},
    { Plt::NWexley_YW, Plt::DVander, Plt::HSalm, Plt::EVerlaine, Plt::GoldSqVet, Plt::GraySqBomber },
    {
      Upg::R5Astro,
      Upg::ProtonBmb, Upg::SeismicCh,
      Upg::VetTurret,
      Upg::ExpHan,
      Upg::IonCanTrt,
    },
    {},
    { TK(Disarm), TS(Ion,3), TS(Shield,2), TS(ProtonBomb,2), TS(SeismicCharge,2)}
  },

  { Rel::SWZ14, "SWZ14", "841333106065", "TIE/ln Fighter Expansion Pack", RelGroup::Wave1,
    {2018, 5, 1}, {2018, 9,13},
    {"https://www.fantasyflightgames.com/en/news/2018/5/1/crush-the-rebellion-1/",
     {"https://www.fantasyflightgames.com/en/news/2018/7/16/space-superiority-1/"},
     ""},
    {{Shp::TIEFighter, ""}},
    {
      Plt::Howlrunner, Plt::MMithel, Plt::SSkutu, Plt::DMeeko, Plt::GHask,
      Plt::SMarana, Plt::BlackSqAce, Plt::ObsidianSqPlt, Plt::AcademyPlt, Plt::Wampa
    },
    {
       Upg::StealthDev,
       Upg::CrackShot, Upg::Juke, Upg::Marksman
    },
    {},
    {
      TS(Evade,2), TS(Focus,2), TS(StandardCharge,2)
    }
  },

  { Rel::SWZ15, "SWZ15", "841333106072", "TIE Advanced x1 Expansion Pack", RelGroup::Wave1,
    {2018, 5, 1}, {2018, 9,13},
    {"https://www.fantasyflightgames.com/en/news/2018/5/1/crush-the-rebellion-1/",
     {"https://www.fantasyflightgames.com/en/news/2018/7/23/on-the-cutting-edge/"},
     ""},
    {{Shp::TIEAdvX1, ""}},
    { /*Plt::*/ },
    { /*Upg::*/ },
    { /*Cnd::*/ },
    {}
  },

  { Rel::SWZ16, "SWZ16", "841333106089", "Slave I Expansion Pack", RelGroup::Wave1,
    {2018, 5, 1}, {2018, 9,13},
    {"https://www.fantasyflightgames.com/en/news/2018/5/1/become-infamous/",
     {"https://www.fantasyflightgames.com/en/news/2018/7/2/any-methods-necessary-1/"},
     ""},
    {{Shp::Firespray, ""}},
    { Plt::BFett, Plt::EAzzameen, Plt::KScarlet, Plt::KFrost, Plt::KTrelix, Plt::BountyHunter },
    {
      Upg::HLC,
      Upg::BFett,
      Upg::ProxMine, Upg::SeismicCh,
      Upg::VetTailGun,
      Upg::InertDamp,
      Upg::ConcusMsl,
      Upg::LoneWolf, Upg::PerCPilot,
      Upg::Andrasta, Upg::Marauder, Upg::SlaveI
     },
    {},
    {
      TK(Reinforce), TS(Shield,4), TS(StandardCharge,3),
      TS(ProximityMine,2), TS(SeismicCharge,2)
    }
  },

  { Rel::SWZ17, "SWZ17", "841333106096", "Fang Fighter Expansion Pack", RelGroup::Wave1,
    {2018, 5, 1}, {2018, 9,13},
    {"https://www.fantasyflightgames.com/en/news/2018/5/1/become-infamous/",
     {"https://www.fantasyflightgames.com/en/news/2018/7/9/direct-confrontation/"},
     ""},
    {{Shp::FangFighter, ""}},
    { Plt::FRau_FF, Plt::JRekkoff, Plt::KSolus, Plt::OTeroch, Plt::SkullSqPlt, Plt::ZealousRecruit },
    {
      Upg::Afterburn,
      Upg::Daredevil,
      Upg::Fearless,
      Upg::IonTrp
    },
    { },
    {
      TK(Crit), TK(Focus), TS(Ion,3), LK(10), ID(10), TS(StandardCharge,2), TK(Stress)
    }
  },

/*
  { Rel::, "", "", "", RelGroup::,
    {0000,00,00}, {0000,00,00},
    {"",
     {},
     ""},
    {{Shp::, ""}},
    { Plt:: },
    { Upg:: },
    { Cnd:: },
    //{}
  },
*/




};

}
