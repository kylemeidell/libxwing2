#include "condition.h"
#include "release.h"

namespace libxwing2 {



std::vector<Condition> Condition::conditions = {
  { Cnd::Hunted,  1, "Hunted",              "After you are destroyed, you must choose another friendly ship and assign this condition to it, if able." },
  { Cnd::LstnDev, 1, "Listening Device",    "During the System Phase, if an enemy ship with the Informant upgrade is at range 0-2, flip your dial faceup." },
  { Cnd::OptProt, 1, "Optimized Prototype", "While you perform a {FRONTARC} primary attack against a ship locked by a friendly ship with the Director Krennic upgrade, you may spend 1 {HIT}/{CRIT}/{FOCUS} result.  If you do, choose one: the defender loses 1 shield, or the defender flips 1 of its facedown damage cards."},
  { Cnd::SupFire, 1, "Suppressive Fire",    "While you perform an attack against a ship other than Captain Rex, roll 1 fewer attack die.  After Captain Rex defends, remove this card.  At the end of the Combat phase, if Captain Rex did not perform an attack this phase, remove this card.  After Captain Rex is destroyed, remove this card." },

};



ConditionNotFound::ConditionNotFound(Cnd c) : runtime_error("Condition not found (enum " + std::to_string((int)c) + ")") { }



Condition::Condition(Cnd         t,
		     uint8_t     l,
		     std::string n,
		     std::string txt)
  : type(t), limited(l), name(n), text(txt) { }



Condition Condition::GetCondition(Cnd t) {
  for(Condition c : Condition::conditions) {
    if(c.GetType() == t) {
      return c;
    }
  }
  throw ConditionNotFound(t);
}

std::vector<Condition> Condition::GetAllConditions() {
  return Condition::conditions;
}

Cnd         Condition::GetType()      const { return this->type; }
uint8_t     Condition::GetLimited()   const { return this->limited; }
std::string Condition::GetName()      const { return this->name; }
std::string Condition::GetText()      const { return this->text; }
bool        Condition::IsUnreleased() const {
  for(const Release& r : Release::GetAllReleases()) {
    if(!r.IsUnreleased()) {
      for(Cnd c : r.GetConditions()) {
	if(c == this->GetType()) {
	  return false;
	}
      }
    }
  }
  return true;
}
  
}
