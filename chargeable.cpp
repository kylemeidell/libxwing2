#include "chargeable.h"

Chargeable::Chargeable(int8_t cap, bool rech)
  : capacity(cap), isRechargeable(rech) { }

int8_t Chargeable::GetCapacity()      { return this->capacity; }
bool   Chargeable::IsRechargeable()   { return this->isRechargeable;   }
