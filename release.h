#pragma once
#include "condition.h"
#include "pilot.h"
#include "ship.h"
#include "token.h"
#include "upgrade.h"
#include <string>

namespace libxwing2 {



enum class Rel {
  SWX74, // saw's renegades
  SWX75, // tie reaper
  SWZ01, // core set

  SWZ04, // landos millennium falcon
//SWZ05, // dice pack
  SWZ06, // rebel conversion kit
  SWZ07, // empire conversion kit
  SWZ08, // scum conversion kit
//SWZ09, // rebel dial upgrade
//SWZ10, // empire dual upgrade
//SWZ11, // scum dial upgrade
  SWZ12, // t65 xwing
  SWZ13, // btla4 ywing
  SWZ14, // tie fighter
  SWZ15, // tie advanced x1
  SWZ16, // slave 1
  SWZ17, // fang fighter
};

class ReleaseNotFound : public std::runtime_error {
 public:
  ReleaseNotFound(Rel r);
  ReleaseNotFound(std::string r);
};

struct RelShip {
  Shp         ship;
  std::string desc; // leave blank for standaard, add text if alternate paint (ex. aces)
};

enum class RelGroup {
  FeWave14,
  CoreSet,
  ConvKit,
  Wave1,
};

struct RelDate {
  unsigned short year;
  unsigned char  month;
  unsigned char  date;
};

struct RelUrls {
  std::string              announcement;
  std::vector<std::string> previews;
  std::string              release;
};



class Release {
 public:
  static Release              GetRelease(Rel r);
  static Release              GetRelease(std::string sku);
  static std::vector<Release> GetByPilot(Plt p);
  static std::vector<Release> GetByUpgrade(Upg u);
  static std::vector<Release> GetAllReleases();

  Rel                      GetType()            const;
  std::string              GetSku()             const;
  std::string              GetIsbn()            const;
  std::string              GetName()            const;
  RelGroup                 GetGroup()           const;
  RelDate                  GetAnnounceDate()    const;
  RelDate                  GetReleaseDate()     const;
  std::string              GetAnnouncementUrl() const;
  std::vector<std::string> GetPreviewUrls()     const;
  std::string              GetReleaseUrl()      const;
  std::vector<RelShip>     GetShips()           const;
  std::vector<Plt>         GetPilots()          const;
  std::vector<Upg>         GetUpgrades()        const;
  std::vector<Cnd>         GetConditions()      const;
  TokenCollection          GetTokens()          const;
  bool                     IsUnreleased()       const;

 private:
  Rel         release;
  std::string sku;
  std::string isbn;
  std::string name;
  RelGroup group;
  RelDate announceDate;
  RelDate releaseDate;
  RelUrls urls;

  std::vector<RelShip> ships;
  std::vector<Plt>     pilots;
  std::vector<Upg>     upgrades;
  std::vector<Cnd>     conditions;
  TokenCollection      tokens;

  static std::vector<Release> releases;

  Release(Rel                       rel,
	  std::string               sku,
          std::string               isbn,
          std::string               nam,
          RelGroup                  gr,
          RelDate                   ad,
          RelDate                   rd,
          RelUrls                   url,
          std::vector<RelShip>      shps,
          std::vector<Plt>          plts,
          std::vector<Upg>          upgs,
          std::vector<Cnd>          cond,
          std::vector<std::shared_ptr<Tokens>> tok);
};

}
