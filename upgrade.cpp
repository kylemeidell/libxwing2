#include "upgrade.h"
#include "release.h"
#include "shared.h"
#include <algorithm>

namespace libxwing2 {

// *** Upgrade ***
UpgradeNotFound::UpgradeNotFound(Upg u) : runtime_error("Upgrade not found (enum: " + std::to_string((int)u) + ")") { }

// factories
Upgrade Upgrade::GetUpgrade(Upg upgrade) {
  for(Upgrade &us : Upgrade::upgrades) {
    if(us.GetType() == upgrade) {
      return us;
    }
  }
  throw UpgradeNotFound(upgrade);
}

std::vector<Upgrade> Upgrade::FindUpgrade(std::string u, std::vector<Upgrade> src) {
  std::vector<Upgrade> ret;
  std::string ss = ToLower(u); // searchString
  for(Upgrade &us : src) {
    if((ToLower(us.GetName()).find(ss)       != std::string::npos) ||
       (ToLower(us.GetTitle()).find(ss)      != std::string::npos) ||
       (ToLower(us.GetShortTitle()).find(ss) != std::string::npos)) {
      ret.push_back(us);
    }
  }
  return ret;
}

std::vector<Upgrade> Upgrade::GetAllUpgrades() { return Upgrade::upgrades; }

// maintenance
void Upgrade::SanityCheck() {
  /*
  typedef std::tuple<std::string, std::string> Entry;
  std::vector<Entry> entries;
  std::vector<Entry> dupes;
  int counter=0;
  printf("Checking Upgrades");
  for(Upgrade u : upgrades) {
    counter++;
    Entry e = Entry{u.GetType().GetXws(), u.GetXws() };
    if(std::find(entries.begin(), entries.end(), e) == entries.end()) {
      printf("."); fflush(stdout);
    } else {
      dupes.push_back(e);
      printf("X"); fflush(stdout);
    }
    entries.push_back(e);
  }
  printf("DONE\n");
  printf("Upgrades: %zu\n", entries.size());
  printf("Dupes: %zu\n", dupes.size());
  if(dupes.size()) {
    for(Entry e : dupes) {
      printf("  %s - %s\n", std::get<0>(e).c_str(), std::get<1>(e).c_str());
    }
  }
  */
}

// info (card)
Upg         Upgrade::GetType()      const { return this->type; }
std::string Upgrade::GetName()      const { return (this->name=="") ? this->sides[0].title : this->name; }
uint8_t     Upgrade::GetLimited()   const { return this->limited; }
bool        Upgrade::IsUnreleased() const {
  for(const Release& r : Release::GetAllReleases()) {
    if(!r.IsUnreleased()) {
      for(Upg u : r.GetUpgrades()) {
	if(u == this->GetType()) {
	  return false;
	}
      }
    }
  }
  return true;
}

// info (side)
UpgradeType      Upgrade::GetUpgradeType() const { return UpgradeType::GetUpgradeType(this->sides[this->curSide].upgradeType); }
std::vector<UpT> Upgrade::GetSlots()       const { return this->sides[this->curSide].slots; }
std::string      Upgrade::GetTitle()       const { return this->sides[this->curSide].title; }
std::string      Upgrade::GetShortTitle()  const { return this->sides[this->curSide].titleShort; }
Chargeable       Upgrade::GetCharge()      const { return this->sides[this->curSide].charge; }
Chargeable       Upgrade::GetForce()       const { return this->sides[this->curSide].force; }
std::experimental::optional<SecAttack> Upgrade::GetAttackStats()    const { return this->sides[this->curSide].attackStats; }
Modifier         Upgrade::GetModifier()        const { return this->sides[this->curSide].modifier; }
Restriction      Upgrade::GetRestriction()     const { return this->sides[this->curSide].restriction; }
bool             Upgrade::HasAbility()         const { return this->sides[this->curSide].hasAbility; }
std::string      Upgrade::GetText()            const { return this->sides[this->curSide].text; }

//std::experimental::optional<AttackStats> Upgrade::GetAttackStats()    const {return this->sides[this->curSide].attackStats; }
//std::experimental::optional<uint8_t>     Upgrade::GetEnergyLimit()    const {return this->sides[this->curSide].energyLimit; }
//std::experimental::optional<int8_t>      Upgrade::GetEnergyModifier() const {return this->sides[this->curSide].energyModifier; }

// stats
/*
std::vector<Upg> Upgrade::GetReqSlots()         const { return this->slots; }
  //StatModifiers    Upgrade::GetStatModifier()     const { return this->sides[this->curSide].statModifier; }
  //ManModifiers     Upgrade::GetManModifier()      const { return this->sides[this->curSide].manModifier; }
RestrictionCheck Upgrade::GetRestrictionCheck() const { return this->restrictionCheck; }
uint8_t          Upgrade::GetExtras()           const { return this->extras; }
void             Upgrade::SetExtras(uint8_t x)        { this->extras = x; }
void             Upgrade::ExtraUp()                   { if(this->extras < 255) { this->extras++; } }
void             Upgrade::ExtraDn()                   { if(this->extras >   0) { this->extras--; } }
*/

// state
bool Upgrade::IsEnabled() const    { return this->isEnabled; }
void Upgrade::Enable()             { this->isEnabled = true; }
void Upgrade::Disable()            { this->isEnabled = false; }
bool Upgrade::IsDualSided() const  { return this->sides.size() > 1; }
void Upgrade::Flip() {
  if(this->sides.size() > 1) {
    this->curSide = (this->curSide+1) % 2;
  }
}



// constructor
  Upgrade::Upgrade(Upg         upg,
		   std::string nam,
		   uint8_t     lim,
		   UpgradeSide s1)
    : type(upg), name(nam), limited(lim), curSide(0), sides({{s1}}) { }

  Upgrade::Upgrade(Upg         upg,
		   std::string nam,
		   uint8_t     lim,
		   UpgradeSide s1,
		   UpgradeSide s2)
    : type(upg), name(nam), limited(lim), curSide(0), sides({{s1,s2}}) { }

}
