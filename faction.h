#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

enum class Fac {
  None       = 0x00,
  Rebel      = 0x01,
  Imperial   = 0x02,
  Scum       = 0x04,
  Resistance = 0x08,
  FirstOrder = 0x10,
  All        = 0x1F,
};

Fac operator|(Fac a, Fac b);
Fac operator&(Fac a, Fac b);

class FactionNotFound : public std::runtime_error {
 public:
  FactionNotFound(Fac f);
  FactionNotFound(std::string xws);
};

class Faction {
 public:
  static Faction GetFaction(Fac s);
  static std::vector<Faction> GetAllFactions();
  Fac         GetType()      const;
  std::string GetName()      const;
  std::string GetShort()     const;

 private:
  Fac type;
  std::string xws;
  std::string name;
  std::string shortName;

  static std::vector<Faction> factions;

  Faction(Fac         t,
          std::string n,
	  std::string s);
};

}
