#include "ship.h"
#include "shared.h"
#include <string>

namespace libxwing2 {

// Ship
ShipNotFound::ShipNotFound(Shp s) : runtime_error("Ship not found (enum " + std::to_string((int)s) + ")") { }

Ship Ship::GetShip(Shp typ) {
  for(Ship s : Ship::ships) {
    if(s.GetType() == typ) {
      return s;
    }
  }
  throw ShipNotFound(typ);
}

std::vector<Ship> Ship::FindShip(std::string s) {
  std::vector<Ship> ret;
  std::string ss = ToLower(s); // searchString
  for(Ship &sh : Ship::ships) {
    if((ToLower(sh.GetName()).find(ss)      != std::string::npos) ||
       (ToLower(sh.GetShortName()).find(ss) != std::string::npos)
       ) {
      ret.push_back(sh);
    }
  }
  return ret;
}

std::vector<Ship> Ship::GetAllShips() { return Ship::ships; }

Shp         Ship::GetType()       const { return this->type; }
std::string Ship::GetName()       const { return this->name; }
std::string Ship::GetShortName()  const { return this->shortName; }
BaseSize    Ship::GetBaseSize()   const { return BaseSize::GetBaseSize(this->baseSize); }
Maneuvers   Ship::GetManeuvers()  const { return this->maneuvers; }

Ship::Ship(Shp         t,
           std::string n,
           std::string s,
           BSz         b,
           Maneuvers   m)
  : type(t), name(n), shortName(s), baseSize(b), maneuvers(m) { }

}
