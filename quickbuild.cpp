#include "quickbuild.h"

namespace libxwing2 {

std::vector<QuickBuild> QuickBuild::quickBuilds = {
  // FangFighter (Scum)
  { Plt::FRau_FF,        3, { Upg::Daredevil, Upg::HullUpg, Upg::Afterburn } },
  { Plt::JRekkoff,       3, { Upg::Predator, Upg::Afterburn, Upg::IonTrp, Upg::HullUpg } },
  { Plt::KSolus,         2, { Upg::Fearless } },
  { Plt::OTeroch,        2, { } },
  { Plt::SkullSqPlt,     2, { Upg::Fearless } },
  { Plt::ZealousRecruit, 2, { Upg::ProtTrp } },
  // Firespray (Scum)
  { Plt::BFett,          4, { Upg::LoneWolf, Upg::SeismicCh, Upg::PerCPilot, Upg::SlaveI, Upg::InertDamp } },
  { Plt::EAzzameen,      4, { Upg::Elusive, Upg::ProxMine, Upg::PerCPilot, Upg::SeismicCh, Upg::InertDamp, Upg::Andrasta } },
  { Plt::BountyHunter,   3, { Upg::PerCPilot, Upg::SeismicCh, Upg::InertDamp } },
  { Plt::KFrost,         3, { Upg::PerCPilot } },
  { Plt::KScarlet,       3, { Upg::Marauder } },
  { Plt::KTrelix,        3, { Upg::ConcusMsl } },
  // T65Xwing (Rebel)
  { Plt::JPorkins,       3, { Upg::Outmaneuv, Upg::HullUpg, Upg::R5D8, Upg::SFoils, Upg::Afterburn } },
  { Plt::LSkywalker,     3, { Upg::InstAim, Upg::R2D2, Upg::ProtTrp, Upg::SFoils } },
  { Plt::TKyrell,        3, { Upg::Elusive, Upg::Afterburn, Upg::IonTrp, Upg::HullUpg, Upg::R2Astro, Upg::SFoils } },
  { Plt::WAntilles,      3, { Upg::Outmaneuv, Upg::ShieldUpg, Upg::ProtTrp, Upg::SFoils, Upg::R4Astro } },
  { Plt::BDarklighter,   2, { Upg::Selfless, Upg::SFoils } },
  { Plt::BlueSqEsc,      2, { Upg::ProtTrp, Upg::SFoils, Upg::R3Astro } },
  { Plt::GDreis_XW,      2, { Upg::SFoils } },
  { Plt::RedSqVet,       2, { Upg::Predator, Upg::SFoils, Upg::R5Astro } },
  // TIEAdvX1 (Imperial)
  { Plt::DVader,         4, { Upg::SNReflex, Upg::ClustMsl, Upg::FCS, Upg::Afterburn, Upg::ShieldUpg } },
  { Plt::MStele,         3, { Upg::Ruthless, Upg::ClustMsl, Upg::FCS, Upg::ShieldUpg } },
  // TIEFighter (Imperial)
  { Plt::BlackSqAce,     2, { Upg::Outmaneuv, Upg::ShieldUpg, Upg::Afterburn } },
  { Plt::DMeeko,         2, { Upg::Juke, Upg::StealthDev } },
  { Plt::GHask,          2, { Upg::CrackShot, Upg::ShieldUpg } },
  { Plt::Howlrunner,     2, { Upg::Juke, Upg::ShieldUpg } },
  { Plt::IVersio,        2, { Upg::Outmaneuv, Upg::ShieldUpg } },
  { Plt::NightBeast,     2, { Upg::Predator, Upg::ShieldUpg, Upg::HullUpg } },
  { Plt::SMarana,        2, { Upg::Marksman, Upg::Afterburn } },
  { Plt::SSkutu,         2, { Upg::Predator, Upg::ShieldUpg } },
  { Plt::VRudor,         2, { Upg::Elusive, Upg::ShieldUpg } },
  { Plt::Wampa,          2, { Upg::CrackShot, Upg::StealthDev, Upg::HullUpg } },
  { Plt::AcademyPlt,     1, { } },
  { Plt::ObsidianSqPlt,  1, { } },
  // YWing (Rebel)
  { Plt::HSalm,          3, { Upg::Outmaneuv, Upg::VetTurret, Upg::IonCanTrt, Upg::R5Astro } },
  { Plt::NWexley_YW,     3, { Upg::ExpHan, Upg::VetTurret, Upg::IonCanTrt, Upg::R3Astro } },
  { Plt::DVander,        2, { Upg::ProtTrp, Upg::R3Astro } },
  { Plt::EVerlaine,      2, { Upg::ExpHan, Upg::IonCanTrt } },
  { Plt::GoldSqVet,      2, { Upg::ExpHan, Upg::R3Astro, Upg::ProtTrp } },
  { Plt::GraySqBomber,   2, { Upg::IonCanTrt, Upg::R5Astro, Upg::ProtonBmb } },
};

QuickBuildNotFound::QuickBuildNotFound(Plt p) : runtime_error("QuickBuild not found: (enum " + std::to_string((int)p) + ")") { }

QuickBuild QuickBuild::GetQuickBuild(Plt plt) {
  for(QuickBuild q : QuickBuild::quickBuilds) {
    if(q.GetPlt() == plt) {
      return q;
    }
  }
  throw QuickBuildNotFound(plt);
}

std::vector<QuickBuild> QuickBuild::GetAllQuickBuilds() {
  return QuickBuild::quickBuilds;
}

Plt                  QuickBuild::GetPlt()      const { return this->pilot; }
Pilot                QuickBuild::GetPilot()    const { return Pilot::GetPilot(this->pilot); }
uint8_t              QuickBuild::GetThreat()   const { return this->threat; }
std::vector<Upgrade> QuickBuild::GetUpgrades() const { std::vector<Upgrade> r; for(Upg u : this->upgrades) { r.push_back(Upgrade::GetUpgrade(u)); };  return r; }

QuickBuild::QuickBuild(Plt              p,
		       uint8_t          t,
		       std::vector<Upg> u)
  : pilot(p), threat(t), upgrades(u) { }

}
