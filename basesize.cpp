#include "basesize.h"

namespace libxwing2 {

// Base Size
std::vector<BaseSize> BaseSize::baseSizes = {
  { BSz::None,   "None"   },
  { BSz::Small,  "Small"  },
  { BSz::Medium, "Medium" },
  { BSz::Large,  "Large"  },
  { BSz::Huge,   "Huge"   },
  { BSz::All,    "All"    },
};

BSz operator|(BSz a, BSz b) {
  return static_cast<BSz>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
}

BSz operator&(BSz a, BSz b) {
  return static_cast<BSz>(static_cast<uint32_t>(a) & static_cast<uint32_t>(b));
}

BaseSizeNotFound::BaseSizeNotFound(BSz b) : runtime_error("Base Size not found (enum " + std::to_string((int)b) + ")") { }

BaseSize BaseSize::GetBaseSize(BSz typ) {
  for(BaseSize b : BaseSize::baseSizes) {
    if(b.GetType() == typ) {
      return b;
    }
  }
  throw BaseSizeNotFound(typ);
}

BSz         BaseSize::GetType()      const { return this->type; }
std::string BaseSize::GetName()      const { return this->name; }

BaseSize::BaseSize(BSz         t,
                   std::string n)
  : type(t), name(n) { }

}
